// 预定实体

const Entity = require('./Entity')

class Preordain extends Entity {
  constructor (options = {}) {
    super('Preordain', options)
    const {
      id, sourceId, userId, roomId, roomTypeId, name, phone, card,
      date, dateEnd, content, status, source, user, room, roomType
    } = options
    this.id         = id         ? Number(id)         : -1   // 预定ID
    this.sourceId   = sourceId   ? Number(sourceId)   : -1   // 房源ID
    this.userId     = userId     ? Number(userId)     : -1   // 用户ID
    this.roomId     = roomId     ? Number(roomId)     : -1   // 房间ID
    this.roomTypeId = roomTypeId ? Number(roomTypeId) : -1   // 户型ID
    this.name       = name       ? String(name)       : ''   // 预定者名字
    this.phone      = phone      ? String(phone)      : ''   // 预定者手机号
    this.card       = card       ? String(card)       : ''   // 身份证
    this.date       = date       ? Number(date)       : 0    // 预定生成时间
    this.dateEnd    = dateEnd    ? Number(dateEnd)    : 0    // 预定入住时间
    this.content    = content    ? String(content)    : ''   // 额外内容
    this.status     = status     ? Number(status)     : 0    // 状态: 0=待交定金 1=已预定 2=已签约 3=未赴约 4=取消预订
    // 非bean属性
    this.source   = source   || null // 房源实体
    this.user     = user     || null // 入住用户实体
    this.room     = room     || null // 房间实体
    this.roomType = roomType || null // 户型实体
  }
}

module.exports = Preordain
