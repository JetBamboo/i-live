// 维修实体

const Entity = require('./Entity')

class Repair extends Entity {
  constructor (options = {}) {
    super('Repair', options)
    this.id         = options.id         ? Number(options.id)         : -1 // 维修ID
    this.userId     = options.userId     ? Number(options.userId)     : -1 // 用户ID
    this.roomId     = options.roomId     ? Number(options.roomId)     : -1 // 房间ID
    this.sourceId   = options.sourceId   ? Number(options.sourceId)   : -1 // 房源ID
    this.roomTypeId = options.roomTypeId ? Number(options.roomTypeId) : -1 // 户型ID
    this.title      = options.title      ? String(options.title)      : '' // 维修标题
    this.area       = options.area       ? String(options.area)       : '' // 维修区域
    this.date       = options.date       ? Number(options.date)       : 0  // 报修发起时间
    this.dateStart  = options.dateStart  ? Number(options.dateStart)  : 0  // 期望维修时间
    this.picture    = options.picture    ? String(options.picture)    : '' // 封面图
    this.content    = options.content    ? String(options.content)    : '' // 描述
    this.status     = options.status     ? Number(options.status)     : 0  // 状态 0=等待维修 1=修复完成 2=取消
    this.access     = typeof options.access === 'boolean'
                    ? options.access
                    : false                     // 是否允许无人时进入房间
    // 非bean属性
    this.user      = options.user      || null // 用户实体
    this.room      = options.room      || null // 房间实体
    this.source    = options.source    || null // 房源
    this.roomType  = options.roomType  || null // 户型
  }
}

module.exports = Repair
