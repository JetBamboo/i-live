// 定金实体

const Entity = require('./Entity')

class BillDeposit extends Entity {
  constructor (options = {}) {
    super('BillDeposit', options)
    this.id     = options.id     ? Number(options.id)     : -1 // 定金ID
    this.billId = options.billId ? Number(options.billId) : -1 // 账单ID
    this.date   = options.date   ? Number(options.date)   : -1 // 生成时间
    this.price  = options.price  ? Number(options.price)  : -1 // 价格
    this.status = options.status ? Number(options.status) : 0  // 状态 0=未支付 1=已支付
    // 非bean属性
    this.bill = options.bill || null // 账单实体
  }
}

module.exports = BillDeposit
