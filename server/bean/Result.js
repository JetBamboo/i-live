const { CODE_SUCCESS } = require('../config')

class Result {
  constructor (value, code, desc) {
    this.value = value || {}            // 值, 可能为数组或是对象
    this.code  = code  || CODE_SUCCESS  // 返回码, CODE_SUCCESS=成功, CODE_ERROR=失败
    this.desc  = desc  || ''            // 解释
  }
}

module.exports = Result
