// 账单实体

const Entity = require('./Entity')

class Bill extends Entity {
  constructor (options = {}) {
    super('Bill', options)
    this.id         = options.id         ? Number(options.id)         : -1 // 账单ID
    this.userId     = options.userId     ? Number(options.userId)     : -1 // 用户ID
    this.roomId     = options.roomId     ? Number(options.roomId)     : -1 // 房间ID
    this.roomTypeId = options.roomTypeId ? Number(options.roomTypeId) : -1 // 户型ID
    this.sourceId   = options.sourceId   ? Number(options.sourceId)   : -1 // 房源ID
    this.date       = options.date       ? Number(options.date)       : 0  // 账单待交日期
    this.status     = options.status     ? Number(options.status)     : 0  // 状态 0=待缴纳 1=已缴纳 2=逾期
    this.type       = options.type       ? Number(options.type)       : 0  // 类型 0=预定账单 1=签约账单 2=合同账单
    // 非bean属性
    this.user        = options.user        || null // 用户实体
    this.roomType    = options.roomType    || null // 户型实体
    this.source      = options.source      || null // 房源实体
    this.room        = options.room        || null // 房间实体
    this.waterList   = options.waterList   || null // 水费清单实体
    this.elecList    = options.elecList    || null // 电费清单实体
    this.depositList = options.depositList || null // 定金清单实体
    this.rentList    = options.rentList    || null // 住房金实体
  }
}

module.exports = Bill
