// LOG实体

const Entity = require('./Entity')

class Log extends Entity {
  constructor (options = {}) {
    super('Log', options)
    this.id      = options.id      || -1 // logID
    this.file    = options.file    || '' // 文件名
    this.content = options.content || '' // 内容
    this.date    = options.date    || 0  // 时间戳
  }
}

module.exports = Log