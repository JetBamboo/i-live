// 房间实体

const Entity = require('./Entity')

class Room extends Entity {
  constructor (options = {}) {
    super('Room', options)
    this.id       = options.id       ? Number(options.id)       : -1 // 房间
    this.typeId   = options.typeId   ? Number(options.typeId)   : -1 // 户型ID
    this.sourceId = options.sourceId ? Number(options.sourceId) : -1 // 房源ID
    this.name     = options.name     ? String(options.name)     : '' // 房间名
    this.code     = options.code     ? String(options.code)     : '' // 房间代码
    this.password = options.password ? String(options.password) : '' // 房间密码
    this.floor    = options.floor    ? String(options.floor)    : '' // 楼层
    this.status   = options.status   ? Number(options.status)   : 0  // 状态 0=空闲 1=已预定 2=租赁中 10=不可用
    this.ownerId  = options.ownerId  ? Number(options.ownerId)  : -1 // 用户ID
    // 非bean属性
    this.source   = options.source   || null // 房源实体
    this.type     = options.type     || null // 户型实体
  }
}

module.exports = Room
