// 活动参与实体

const Entity = require('./Entity')

class ActivityParticipate extends Entity {
  constructor (options = {}) {
    super('ActivityParticipate', options)
    this.id         = options.id         ? Number(options.id)         : -1   // 参与表ID
    this.userId     = options.userId     ? Number(options.userId)     : -1   // 用户ID
    this.activityId = options.activityId ? Number(options.activityId) : -1   // 活动ID
    this.date       = options.date       ? Number(options.date)       : 0    // 参与时间
    this.status     = options.status     ? Number(options.status)     : 0    // 状态 0=已参与 1=已结束 2=已取消
    // 非bean属性
    this.user     = options.user     || null // 用户实体
    this.activity = options.activity || null // 活动实体
  }
}

module.exports = ActivityParticipate
