// 预约实体

const Entity = require('./Entity')

class Reservation extends Entity {
  constructor (options = {}) {
    super('Reservation', options)
    this.id        = options.id        || -1   //预约ID
    this.sourceId  = options.sourceId  || -1   // 房源ID
    this.typeId    = options.typeId    || -1   // 户型ID
    this.userId    = options.userId    || -1   // 用户ID
    this.name      = options.name      || ''   // 预约名
    this.phone     = options.phone     || ''   // 预约手机号
    this.date      = options.date      || 0    // 预约发起时间
    this.dateStart = options.dateStart || 0    // 预约看房时间
    this.dateLive  = options.dateLive  || ''    // 期望入住时间
    this.budget    = options.budget    || ''   // 房租预算
    this.content   = options.content   || ''   // 期望楼层或朝向
    this.status    = options.status    || 0    // 状态 0=已预约 1=已结束 2=未赴约 3=取消预约
    // 非bean属性
    this.source    = options.source    || null // 房源实体
    this.roomType  = options.roomType  || null // 户型实体
    this.user      = options.user      || null // 用户实体
  }
}

module.exports = Reservation