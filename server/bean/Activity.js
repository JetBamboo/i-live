// 活动实体

const Entity = require('./Entity')

class Activity extends Entity {
  constructor (options = {}) {
    super('Activity', options)
    this.id          = options.id          ? Number(options.id)          : -1 // 活动ID
    this.sourceId    = options.sourceId    ? Number(options.sourceId)    : -1 // 房源ID
    this.title       = options.title       ? String(options.title)       : '' // 标题
    this.cover       = options.cover       ? String(options.cover)       : '' // 封面图
    this.datePublish = options.datePublish ? Number(options.datePublish) : '' // 发布时间
    this.dateStart   = options.dateStart   ? Number(options.dateStart)   : 0  // 开始时间
    this.dateEnd     = options.dateEnd     ? Number(options.dateEnd)     : 0  // 结束时间
    this.content     = options.content     ? String(options.content)     : '' // 内容
    // 非bean属性
    this.participates = options.participates || null // 参与情况
    this.source       = options.source       || null // 房源
  }
}

module.exports = Activity
