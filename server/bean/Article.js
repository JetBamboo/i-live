// 文章实体

const Entity = require('./Entity') 

class Article extends Entity {
  constructor (options = {}) {
    super('Article', options)
    this.id       = options.id       ? Number(options.id)       : -1 // 文章ID
    this.sourceId = options.sourceId ? Number(options.sourceId) : -1 // 房源ID
    this.title    = options.title    ? String(options.title)    : '' // 标题
    this.content  = options.content  ? String(options.content)  : '' // 内容
    this.date     = options.date     ? Number(options.date)     : 0  // 发布日期
    this.type     = options.type     ? Number(options.type)     : 0  // 文章类型 0=房源文章 1=用户文章(弃用)
    this.cover    = options.cover    ? String(options.cover)    : '' // 文章封面图
    // 非bean属性
    this.source   = options.source   || null // 房源实体
  }
}

module.exports = Article
