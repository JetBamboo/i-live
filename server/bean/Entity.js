// 实体原型
const loger = require('../utils/loger')
const config = require('../config')

const afterEntityCreate = (_) => setTimeout(_)

class Entity {
  constructor (name = 'noname', options = {}) {
    afterEntityCreate(() => {
      config.LOG_ENTITY && loger.info(`<Entity.js> 创建实体 ${name} ${JSON.stringify(this)}`)
    })
  }
}

module.exports = Entity