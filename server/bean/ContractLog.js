// 合同记录实体

const Entity = require('./Entity')

class ContractLog extends Entity {
  constructor (options = {}) {
    super('ContractLog', options)
    this.id          = options.id       ? Number(options.id)       : -1 // 记录ID
    this.contractId  = options.sourceId ? Number(options.sourceId) : -1 // 合同ID
    this.type        = options.type     ? String(options.type)     : '' // 类型
    this.date        = options.date     ? Number(options.data)     : 0 // 发生时间
  }
}

module.exports = ContractLog
