// 房间类型实体

const Entity = require('./Entity')

class RoomType extends Entity {
  constructor (options = {}) {
    super('RoomType', options)
    this.id         = options.id         || -1   // 户型ID
    this.sourceId   = options.sourceId   || -1   // 房源ID
    this.name       = options.name       || ''   // 户型名
    this.picture    = options.picture    || ''   // 户型封面
    this.desc       = options.desc       || ''   // 户型介绍
    this.furnitures = options.furnitures || ''   // 家具,具体看映射表
    this.size       = options.size       || ''   // 房间大小
    this.type       = options.type       || ''   // 房间格局
    this.price      = options.price      || 0    // 房间价格
    this.source     = options.source     || null // 房源实体
  }
}

module.exports = RoomType
