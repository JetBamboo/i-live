// 建议意见实体

const Entity = require('./Entity')

class Suggestion extends Entity {
  constructor (options = {}) {
    super('Suggestion', options)
    this.id      = options.id      || -1   // 建议ID
    this.roomId  = options.roomId  || -1   // 房间ID
    this.userId  = options.userId  || -1   // 用户ID
    this.content = options.content || ''   // 内容
    this.date    = options.date    || 0    // 创建时间
    this.room    = options.room    || null // 房间实体
    this.user    = options.user    || null // 用户实体
  }
}

module.exports = Suggestion
