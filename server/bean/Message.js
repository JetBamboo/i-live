// 信息实体

const Entity = require('./Entity')

class Message extends Entity {
  constructor (options = {}) {
    super('Message', options)
    this.id          = options.id         || -1   // 消息ID
    this.userId      = options.userId     || -1   // 用户ID
    this.resourceId  = options.resourceId || -1   // 来源实体ID
    this.type        = options            || ''   // 类型 0=未读 1=已读
    this.title       = options.title      || ''   // 标题
    this.content     = options.content    || ''   // 内容
    this.date        = options.date       || 0    // 发生时间
    this.user        = options.user       || null // 用户
    this.resource    = options.resource   || null // 来源实体
  }
}

module.exports = Message
