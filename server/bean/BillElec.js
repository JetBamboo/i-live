// 电费实体

const Entity = require('./Entity')

class BillElec extends Entity {
  constructor (options = {}) {
    super('BillElec', options)
    this.id     = options.id     ? Number(options.id)     : -1 // 电表ID
    this.userId = options.userId ? Number(options.userId) : -1 // 用户ID
    this.billId = options.billId ? Number(options.billId) : -1 // 账单ID
    this.price  = options.price  ? Number(options.price)  : 0  // 电价
    this.status = options.status ? Number(options.status) : 0  // 状态 0=未交 1=已交
    this.date   = options.date   ? Number(options.date)   : 0  // 抄表时间
    // 非bean属性
    this.user = options.user || null // 用户实体
    this.bill = options.bill || null // 账单实体
  }
}

module.exports = BillElec