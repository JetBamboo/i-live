// 优惠券实体

const Entity = require('./Entity')

class Conpon extends Entity {
  constructor (options = {}) {
    super('Conpon', options)
    this.id     = options.id     ? Number(options.id)     : -1 // 优惠券ID
    this.userId = options.userId ? Number(options.userId) : -1 // 用户ID
    this.price  = options.price  ? Number(options.price)  : 0  // 优惠价格
    // 非bean属性
    this.user = options.user || null // 用户实体
  }
}

module.exports = Conpon