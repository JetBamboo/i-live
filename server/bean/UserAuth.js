const Entity = require('./Entity')

class UserAuth extends Entity {
  constructor (options = {}) {
    super('UserAuth', options)
    this.id        = options.id        ? Number(options.id)        : -1 // 认证ID(用户ID)
    this.compId    = options.compId    ? Number(options.compId)    : -1 // 认证方ID
    this.compName  = options.compName  ? String(options.compName)  : '' // 认证机构名
    this.name      = options.name      ? String(options.name)      : '' // 真实姓名
    this.status    = options.status    ? Number(options.status)    : 0  // 认证状态 0=未认证 1=已认证 2=正在认证
    this.date      = options.date      ? Number(options.date)      : 0  // 认证时间
    this.card      = options.card      ? String(options.card)      : '' // 证件号码
    this.cardType  = options.cardType  ? String(options.cardType)  : '' // 证件类型
    this.cardFront = options.cardFront ? String(options.cardFront) : '' // 证件正面照
    this.cardBack  = options.cardBack  ? String(options.cardBack)  : '' // 证件背面照
  } 
}

module.exports = UserAuth
