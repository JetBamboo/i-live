// 合同实体

const Entity = require('./Entity')

class Contract extends Entity {
  constructor (options = {}) {
    super('Contract', options)
    this.id          = options.id          ? Number(options.id)          : -1 // 合约ID
    this.sourceId    = options.sourceId    ? Number(options.sourceId)    : -1 // 房源ID
    this.roomId      = options.roomId      ? Number(options.roomId)      : -1 // 房间ID
    this.userId      = options.userId      ? Number(options.userId)      : -1 // 用户ID
    this.roomTypeId  = options.roomTypeId  ? Number(options.roomTypeId)  : -1 // 户型ID
    this.name        = options.name        ? String(options.name)        : '' // 承租人名字
    this.phone       = options.phone       ? String(options.phone)       : '' // 承租人联系电话
    this.urgentName  = options.urgentName  ? String(options.urgentName)  : '' // 紧急联系人名称
    this.urgentPhone = options.urgentPhone ? String(options.urgentPhone) : '' // 紧急联系人电话
    this.location    = options.location    ? String(options.location)    : '' // 通讯地址
    this.email       = options.email       ? String(options.email)       : '' // 通讯地址
    this.card        = options.card        ? String(options.card)        : '' // 承租人身份证
    this.cardFront   = options.cardFront   ? String(options.cardFront)   : '' // 身份证正面照
    this.cardBack    = options.cardBack    ? String(options.cardBack)    : '' // 身份证反面照
    this.date        = options.date        ? Number(options.date)        : 0  // 合同生成时间
    this.dateStart   = options.dateStart   ? Number(options.dateStart)   : 0  // 合同开始时间
    this.dateEnd     = options.dateEnd     ? Number(options.dateEnd)     : 0  // 合同结束时间
    this.status      = options.status      ? Number(options.status)      : 0  // 状态 0=待交首期款 1=待审核 2=待入住 3=租赁中  
    // 非bean属性                                                              //     4=待退押金 5=合同中止 10=合同未生效
    this.source   = options.source   || null // 房源实体
    this.room     = options.room     || null // 房间实体
    this.roomType = options.roomType || null // 户型实体
    this.user     = options.user     || null // 用户实体
  }
}

module.exports = Contract
