const Entity = require('./Entity')

class Source extends Entity {
  constructor (options = {}) {
    super('Source', options)
    this.id            = options.id            || -1   // 房源ID
    this.name          = options.name          || ''   // 房源名称
    this.desc          = options.desc          || ''   // 房源介绍
    this.phone         = options.phone         || ''   // 联系电话
    this.picture       = options.picture       || ''   // 封面
    this.city          = options.city          || ''   // 所在城市
    this.coordinate    = options.coordinate    || ''   // 坐标
    this.nearby        = options.nearby        || ''   // 设施,具体看映射表
    this.location      = options.location      || ''   // 所在位置
    this.deposit       = options.deposit       || 0    // 定金
    // 非bean属性
    this.rate          = options.rate          || 0    // 星级
    this.roomTypeList  = options.roomTypeList  || null // 户型实体表
    this.roomList      = options.roomList      || null // 房间实体表
  }
}

module.exports = Source
