const Entity = require('./Entity')

class User extends Entity {
  constructor (options = {}) {
    super('User', options)
    this.id           = options.id           ? Number(options.id)           : -1 // 用户ID
    this.avatar       = options.avatar       ? String(options.avatar)       : '' // 用户头像地址
    this.username     = options.username     ? String(options.username)     : '' // 账号
    this.name         = options.name         ? String(options.name)         : '' // 用户名
    this.phone        = options.phone        ? String(options.phone)        : '' // 联系电话
    this.desc         = options.desc         ? String(options.desc)         : '' // 个性签名
    this.email        = options.email        ? String(options.email)        : '' // 邮箱
    this.status       = options.status       ? Number(options.status)       : 0  // 状态 0=正常 1=冻结
    this.dateCreate   = options.dateCreate   ? Number(options.dateCreate)   : 0  // 账号创建时间
    this.thirdAccount = options.thirdAccount ? String(options.thirdAccount) : '' // 第三方账号
    this.thirdName    = options.thirdName    ? String(options.thirdName)    : '' // 第三方用户名
    // 非bean属性
    this.auth = options.auth || null // 认证信息
  } 
}

module.exports = User
