// 评论实体

const Entity = require('./Entity')

class Comment extends Entity {
  constructor (options = {}) {
    super('Assessment', options)
    this.id           = options.id           ? Number(options.id)           : -1   // 评论ID
    this.userId       = options.userId       ? Number(options.userId)       : -1   // 用户ID
    this.sourceId     = options.sourceId     ? Number(options.sourceId)     : -1   // 房源ID
    this.resourceId   = options.resourceId   ? Number(options.resourceId)   : -1   // 来源ID
    this.resourceType = options.resourceType ? Number(options.resourceType) : 0    // 来源类型 0=账单，1=预约，2=维修
    this.rate         = options.rate         ? Number(options.rate)         : 0    // 星级 {int}
    this.date         = options.date         ? Number(options.date)         : 0    // 发布日期
    this.content      = options.content      ? String(options.content)      : ''   // 评论内容
    // 非bean属性
    this.user     = options.user     || null // 用户实体
    this.source   = options.source   || null // 房源实体
    this.resource = options.resource || null // 来源实体
  }
}

module.exports = Comment
