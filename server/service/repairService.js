const repairDao = require('../dao/repairDao')

/**
 * 通过用户ID获取维修表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Repair>>}
 */
const getRepairList = id => repairDao.getRepairList(id)

// 添加
const add = repair => repairDao.add(repair)

// 用维修ID获取维修信息
const getById = id => repairDao.getById(id)

module.exports = {
  getRepairList,
  add,
  getById
}
