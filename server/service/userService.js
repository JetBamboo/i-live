const userDao = require('../dao/userDao')

/**
 * 通过pin获取用户ID
 * @param {Number|String} pin 
 * @returns {Promise<Number>}
 */
const getUserIdByPin = pin => new Promise(async (resolve, reject) => {
  // 如果不符合类型，则返回null
  if (!['number', 'string'].includes(typeof pin)) {
    return reject(new Error('pin不合法'))
  }
  // 如果为空，则返回null
  if (pin === '' || pin === -1) {
    return reject(new Error('pin不合法'))
  }
  resolve(+pin) // TODO: 需要对pin进行验证
})

/**
 * 获取用户列表
 * @returns {Promise<Array<User>>}
 */
const getUserList = () => userDao.getUserList()

/**
 * 根据用户ID获取用户
 * @param {Number|String} id 用户ID
 * @returns {Promise<User>}
 */
const getUserById = id => userDao.getUserById(id)

/**
 * 根据用户账号获取用户
 * @param {String} username 用户账号
 * @returns {Promise<User>}
 */
const getUserByUsername = username => userDao.getUserByUsername(username)

/**
 * 修改用户信息
 * @param {User} user 用户
 * @returns {Promise<void>}
 */
const editUser = user => userDao.editUser(user)

/**
 * 添加用户信息
 * @param {User} user 用户
 * @returns {Promise<User>}
 */
const addUser = user => userDao.addUser(user)

/**
 * 修改用户头像信息
 * @param {User} user 用户
 * @returns {Promise<void>}
 */
const changeUserAvatar = (id, avatar) => userDao.changeUserAvatar(id, avatar)

/**
 * 通过账号判断用户是否存在
 * @param {String} username 账号
 * @returns {Promise<Boolean>}
 */
const isUserExist = username => new Promise(async resolve => {
  try {
    await userDao.getUserByUsername(username)
    resolve(true)
  } catch (_) {
    resolve(false)
  }
})

/**
 * 通过用户ID获取认证信息
 * @param {String|Number} id 用户ID
 * @returns {Promise<UserAuth>}
 */
const getAuthById = id => userDao.getAuthById(id)

/**
 * 添加新的认证信息
 * @param {UserAuth} auth 
 * @returns {Promise<UserAuth>}
 */
const addAuth = auth => userDao.addAuth(auth)

/**
 * 修改认证
 * @param {UserAuth} auth
 * @returns {Promise<UserAuth>}
 */
const editAuth = auth => userDao.editAuth(auth)

module.exports = {
  getUserIdByPin,
  getUserList,
  getUserById,
  getUserByUsername,
  editUser,
  addUser,
  changeUserAvatar,
  isUserExist,
  getAuthById,
  addAuth,
  editAuth
}