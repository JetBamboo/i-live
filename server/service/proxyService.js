const request = require('request')
const { AMAP_KEY_SUPPORT } = require('../config')
const { resolveParams } = require('../utils/stringUtils')

const getAddressTips = (keywords, city) => new Promise((resolve, reject) => {
  request('https://restapi.amap.com/v3/assistant/inputtips' + resolveParams({
    key: AMAP_KEY_SUPPORT,
    keywords: encodeURIComponent(keywords),
    city
  }), (err, res, body) => {
    if (err) {
        reject(error instanceof Error ? err : new Error(err))
      } else if (res.statusCode === 200) {
        body = JSON.parse(body)
        body.status === '1'
          ? resolve(body.tips)
          : reject(new Error('body.info'))
      } else {
        reject(new Error('unknown error'))
      }
  })
})

module.exports = {
  getAddressTips
}