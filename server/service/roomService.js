const roomDao = require('../dao/roomDao')

/**
 * 根据户型ID获取户型
 * @param {String|Number} id 户型ID
 * @returns {Promise<RoomType>}
 */
const getRoomTypeById = id => roomDao.getRoomTypeById(id)

/**
 * 根据房源ID获取户型列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
const getRoomTypeListBySourceId = id => roomDao.getRoomTypeListBySourceId(id)

/**
 * 根据房源ID获取户型展示列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
const getRoomTypeDisplayListBySourceId = id => roomDao.getRoomTypeDisplayListBySourceId(id)

/**
 * 根据房源ID获取房间列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<Room>>}
 */
const getRoomListBySourceId = id => roomDao.getRoomListBySourceId(id)

/**
 * 根据用户ID获取房间列表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Room>>}
 */
const getRoomListByUserId = id => roomDao.getRoomListByUserId(id)

/**
 * 通过房间ID获取房间
 * @param {String|Number} id 房间ID
 * @returns {Promise<Room>}
 */
const getRoomById = id => roomDao.getRoomById(id)

/**
 * 通过房源ID和code获取房间列表
 * @param {Number|String} id 
 * @param {Number|String} code 房间码
 * @returns {Promise<Array<Room>>}
 */
const getListSafe = (id, code) => roomDao.getListSafe(id, code)

/**
 * 改变房间的状态
 * @param {String|Number} id 房间ID
 * @param {String|Number} status 新状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => roomDao.changeStatus(id, status)

/**
 * 改变房间的所有人
 * @param {String|Number} id 房间ID 
 * @param {String|Number} ownerId 所有人ID
 * @returns {Promise<void>}
 */
const changeOwner = (id, ownerId) => roomDao.changeOwner(id, ownerId)

module.exports = {
  getRoomTypeById,
  getRoomTypeListBySourceId,
  getRoomTypeDisplayListBySourceId,
  getRoomListBySourceId,
  getRoomListByUserId,
  getRoomById,
  getListSafe,
  changeStatus,
  changeOwner
}