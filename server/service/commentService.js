const commentDao = require('../dao/commentDao')

/**
 * 通过房源ID获取评论
 * @param {String|Number} id 
 * @returns {Promise}
 */
const getCommentsBySourceId = id => commentDao.getCommentsBySourceId(id)

module.exports = {
  getCommentsBySourceId
}
