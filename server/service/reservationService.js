const reservationDao = require('../dao/reservationDao')

/**
 * 添加预约
 * @param {Object} reservation 无ID预约单
 * @returns {Promise<Reservation>}
 */
const addReservation = reservation => reservationDao.addReservation(reservation)

/**
 * 获取预约列表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Reservation>>}
 */
const getReservationListByUserId = id => reservationDao.getReservationListByUserId(id)

/**
 * 根据预约ID获取预约
 * @param {String|Number} id 预约ID
 * @returns {Promise<Reservation>}
 */
const getReservationById = id => reservationDao.getReservationById(id)

/**
 * 修改reservation信息
 * @param {Reservation} reservation 预约实体 
 * @returns {Promise<void>}
 */
const editReservation = reservation => reservationDao.editReservation(reservation)

/**
 * 获取所有的预约列表
 * @returns {Promise<Array<Reservation>>}
 */
const getListAll = () => reservationDao.getListAll()

/**
 * 改变预约的状态
 * @param {Number|String} id 预约ID
 * @param {Number|String} status 新状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => reservationDao.changeStatus(id, status)

module.exports = {
  addReservation,
  getReservationListByUserId,
  getReservationById,
  editReservation,
  getListAll,
  changeStatus
}
