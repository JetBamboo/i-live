const logDao = require('../dao/logDao')

const log = (log) => logDao.log(log)

module.exports = {
  log
}