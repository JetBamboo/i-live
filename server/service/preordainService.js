const preordainDao = require('../dao/preordainDao')

/**
 * 通过用户ID获取预约列表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Preordain>>}
 */
const getListByUserId = id => preordainDao.getListByUserId(id)

/**
 * 添加新的预定信息
 * @param {Preordain} preordain 预定实体
 * @returns {Promise<Preordain>}
 */
const add = preordain => preordainDao.add(preordain)

/**
 * 通过ID获取预定
 * @param {Number|String} id 预定ID
 * @returns {Promise<Preordain>}
 */
const getById = id => preordainDao.getById(id)

/**
 * 根据房间ID，用户ID，时间戳寻找支付定金的预定
 * @param {Number|String} roomId 房间ID
 * @param {Number|String} userId 用户ID
 * @param {Number|String} date 创建时间
 * @returns {Promise<void>}
 */
const payDeposit = (roomId, userId, date) => preordainDao.payDeposit(roomId, userId, date)

/**
 * 通过房间ID获取status=1的预定信息
 * @param {Number|String} roomId 房间ID
 * @returns {Promise<Preordain>}
 */
const getEffectByRoomId = roomId => preordainDao.getEffectByRoomId(roomId)

/**
 * 改变状态
 * @param {Number|String} id 预定ID
 * @param {Number|String} status 状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => preordainDao.changeStatus(id, status)

// 通过账单获取预定
const getByBill = bill => preordainDao.getByBill(bill)

// 修改
const edit = preordain => preordainDao.edit(preordain)

// 通过房间ID获取list
const getListByRoomId = id => preordainDao.getListByRoomId(id)

module.exports = {
  getListByUserId,
  add,
  getById,
  payDeposit,
  getEffectByRoomId,
  changeStatus,
  getByBill,
  edit,
  getListByRoomId
}