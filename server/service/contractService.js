const contractDao = require('../dao/contractDao')

/**
 * 添加新合同
 * @param {Contract} contract 
 * @returns {Promise<Contract>}
 */
const add = contract => contractDao.add(contract)

// 通过用户ID获取列表
const getListByUserId = id => contractDao.getListByUserId(id)

// 通过合同id获取合同
const getById = id => contractDao.getById(id)

// 添加合同记录
const addLog = log => contractDao.addLog(log)

// 通过账单的信息获取合同
const getByBill = bill => contractDao.getByBill(bill)

// 改变状态
const changeStatus = (id, status) => contractDao.changeStatus(id, status)

module.exports = {
  add,
  getListByUserId,
  getById,
  addLog,
  getByBill,
  changeStatus
}