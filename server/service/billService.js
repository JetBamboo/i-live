const billDao = require('../dao/billDao')

/**
 * 通过用户的ID获取账单列表
 * @param {Number|String} id 用户ID
 * @returns {Promise<Array<Bill>>}
 */
const getListByUserId = id => billDao.getListByUserId(id)

/**
 * 通过用户的ID获取账单
 * @param {Number|String} id 用户ID
 * @returns {Promise<Bill>}
 */
const getByUserId = id => billDao.getByUserId(id)

/**
 * 通过账单的ID获取账单
 * @param {Number|String} id 账单ID
 * @returns {Promise<Bill>}
 */
const getById = id => billDao.getById(id)

/**
 * 添加新的账单
 * @param {Bill} bill 账单实体
 * @returns {Promise<Bill>}
 */
const add = bill => billDao.add(bill)

/**
 * 添加新的定金账单
 * @param {BillDeposit} deposit 定金实体
 * @returns {Promise<BillDeposit>}
 */
const addDeposit = deposit => billDao.addDeposit(deposit)

/**
 * 添加新的房租账单
 * @param {BillRent} rent 房租实体
 * @returns {Promise<BillRent>}
 */
const addRent = rent => billDao.addRent(rent)

/**
 * 通过账单ID获取水费列表
 * @param {Number|String} billId 账单ID
 * @returns {Promise<Array<Water>>}
 */
const getWaterList = billId => billDao.getWaterList(billId)

/**
 * 通过账单ID获取电费列表
 * @param {Number|String} billId 账单ID
 * @returns {Promise<Array<Electricity>>}
 */
const getElecList = billId => billDao.getElecList(billId)

/**
 * 通过账单ID获取房租列表
 * @param {Number|String} billId 账单ID
 * @returns {Promise<Array<BillRent>>}
 */
const getRentList = billId => billDao.getRentList(billId)

/**
 * 通过账单ID获取定金列表
 * @param {Number|String} billId 账单ID
 * @returns {Promise<Array<Electricity>>}
 */
const getDepositList = billId => billDao.getDepositList(billId)

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeWaterStatusByBill = (billId, status) => billDao.changeWaterStatusByBill(billId, status)

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeElecStatusByBill = (billId, status) => billDao.changeElecStatusByBill(billId, status)

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeDepositStatusByBill = (billId, status) => billDao.changeDepositStatusByBill(billId, status)

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeRentStatusByBill = (billId, status) => billDao.changeRentStatusByBill(billId, status)

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeStatus = (billId, status) => billDao.changeStatus(billId, status)

module.exports = {
  getListByUserId,
  getByUserId,
  getById,
  add,
  addDeposit,
  addRent,
  getWaterList,
  getElecList,
  getRentList,
  getDepositList,
  changeDepositStatusByBill,
  changeElecStatusByBill,
  changeRentStatusByBill,
  changeWaterStatusByBill,
  changeStatus
}
