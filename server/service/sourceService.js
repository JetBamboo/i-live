const sourceDao = require('../dao/sourceDao')

/**
 * 根据房源所在城市来搜索房源
 * @param {String} city 所在城市 
 * @returns {Promise<Array>}
 */
const getSourceListByCity = city => sourceDao.getSourceListByCity(city)

/**
 * 根据所在地推荐房源
 * @param {String} city 所在城市
 * @returns {Promise<Array>}
 */
const getRecommendSourceList = city => sourceDao.getRecommendSourceList(city)

/**
 * 根据ID获取房源
 * @param {String|Number} id 房源ID
 * @returns {Promise<Source>}
 */
const getSourceById = id => sourceDao.getSourceById(id)

/**
 * 根据房源名称获取房源
 * @param {String} name 房源名称
 * @returns {Promise<Array<Source>>}
 */
const getListByName = name => sourceDao.getListByName(name)

module.exports = {
  getSourceListByCity,
  getRecommendSourceList,
  getSourceById,
  getListByName
}