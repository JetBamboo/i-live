const communityDao = require('../dao/communityDao.js')

/**
 * 获取文章列表
 * @returns {Promise}
 */
const getArticleList = _ => communityDao.getArticleList()

/**
 * 根据文章ID获取文章
 * @param {String|Number} id 文章ID 
 * @returns {Promise}
 */
const getArticleById = id => communityDao.getArticleById(id)

/**
 * 获取活动列表
 * @returns {Promise}
 */
const getActivityList = _ => communityDao.getActivityList()

/**
 * 根据活动ID获取活动
 * @param {String|Number} id 文章ID
 * @returns {Promise}
 */
const getActivityById = id => communityDao.getActivityById(id)

/**
 * 通过ID来参加活动
 * @param {ActivityParticipate} ap 活动预约对象
 * @returns {Promise}
 */
const joinActivity = ap => communityDao.joinActivity(ap)

/**
 * 通过活动ID来获取参与列表
 * @param {*} id 活动ID
 * @returns {Promise}
 */
const getAPListByActivityId = id => communityDao.getAPListByActivityId(id)

/**
 * 预约是否已经存在了
 * @param {ActivityParticipate} ap 活动预约实体
 * @returns {Promise}
 */
const isAlreadyParticipate = ap => communityDao.isAlreadyParticipate(ap)

/**
 * 通过用户ID与活动ID来获取活动预约
 * @param {Number|String} userId 用户ID
 * @param {Number|String} activityId 活动ID
 */
const getAPByUserAndActivityId = (userId, activityId) =>
  communityDao.getAPByUserAndActivityId(userId, activityId)

/**
 * 取消活动预约
 * @param {ActivityParticipate} ap 活动参与实体
 */
const quitActivity = ap => communityDao.quitActivity(ap)

/**
 * 添加一条活动预约记录
 * @param {ActivityParticipate} ap 活动参与实体
 */
const addActivityParticipate = ap => communityDao.addActivityParticipate(ap)

module.exports = {
  getArticleList,
  getArticleById,
  getActivityList,
  getActivityById,
  joinActivity,
  getAPListByActivityId,
  isAlreadyParticipate,
  getAPByUserAndActivityId,
  quitActivity,
  addActivityParticipate
}