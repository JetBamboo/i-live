const request = require('request')
const { MAIL_ENABLE } = require('../config')

const createVerifyCode = (length) => {
  return Array.from({ length }, _ => (Math.random() * 10 | 0)).join('')
}

const sendMailVerifyCode = phone => new Promise((resolve, reject) => {
  if (MAIL_ENABLE) {
    const code = createVerifyCode(6)
    request({
      method:'post',
      url: 'https://sms.yunpian.com/v2/sms/single_send.json',
      headers: {
        'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
      },
      form:{
        apikey: 'dd214895c1b99515389251fd4175c4d3',
        mobile: phone,
        text: '【云片网】您的验证码是' + code,
      }
    }, (error, response, body) => {
      if (error) {
        reject(error instanceof Error ? error : new Error(error))
      } else if (response.statusCode === 200) {
        resolve(code)
      } else {
        reject(new Error(body))
      }
    })
  } else {
    resolve(654321)
  }
})

module.exports = {
  sendMailVerifyCode
}