// 必须放置在最前
const __start = new Date().getTime()
const Loger = require('./utils/loger')
const loger = new Loger('app.js')
loger.info('服务器正在启动...')

// 插件
const express = require('express')
const fileUpload = require('express-fileupload')
const cookieParser = require('cookie-parser')
const https = require('https')
const path = require('path')
const fs = require('fs')
const Result = require('./bean/Result')
const { getServerPath } = require('./utils/stringUtils')
const schedule = require('./schedule')

// router
const userRouter        = require('./routes/userRouter')
const sourceRouter      = require('./routes/sourceRouter')
const roomRouter        = require('./routes/roomRouter')
const testRouter        = require('./routes/testRouter')
const uploadRouter      = require('./routes/uploadRouter')
const proxyRouter       = require('./routes/proxyRouter')
const reservationRouter = require('./routes/reservationRouter')
const communityRouter   = require('./routes/communityRouter')
const repairRouter      = require('./routes/repairRouter')
const preordainRouter   = require('./routes/preordainRouter')
const billRouter        = require('./routes/billRouter')
const contractRouter    = require('./routes/contractRouter')

// 配置项
const {
  SERVER_HTTPS_FILES_URL,
  SERVER_STATIC_URL,
  SERVER_ALLOW_ORIGIN,
  SERVER_ALLOW_CREDENTIALS,
  SERVER_API_PREFIX,
  CODE_ERROR,
  CODE_SUCCESS,
  SERVER_PROTOCOL,
  SERVER_PORT,
  SERVER_ADDRESS,
  ENV
} = require('./config')

// 创建express实例
const app = express()

// 创建https服务器
const httpsServer = https.createServer({
  key: fs.readFileSync(path.resolve(__dirname, SERVER_HTTPS_FILES_URL + 'private.pem'), 'utf8'),
  cert: fs.readFileSync(path.resolve(__dirname, SERVER_HTTPS_FILES_URL + 'certificate.crt'), 'utf8')
}, app)

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, SERVER_STATIC_URL)))
app.use(cookieParser())
app.use(fileUpload({
  createParentPath: true,
  useTempFiles : true,
  tempFileDir : path.resolve(__dirname, './temp')
}))
// app.use(express.bodyParse({ uploadDir: './temp' }))

// 设置允许跨域访问该服务
app.all('*', (_, res, next) => {
  res.header('Access-Control-Allow-Origin', SERVER_ALLOW_ORIGIN)
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Credentials', SERVER_ALLOW_CREDENTIALS)
  res.header('Content-Type', 'application/json;charset=utf-8')
  next()
})

app.get('/', (_, res) => res.send('server running'))

// 路由
app.use(`/${SERVER_API_PREFIX}/test`       , testRouter)
app.use(`/${SERVER_API_PREFIX}/user`       , userRouter)
app.use(`/${SERVER_API_PREFIX}/source`     , sourceRouter)
app.use(`/${SERVER_API_PREFIX}/room`       , roomRouter)
app.use(`/${SERVER_API_PREFIX}/upload`     , uploadRouter)
app.use(`/${SERVER_API_PREFIX}/proxy`      , proxyRouter)
app.use(`/${SERVER_API_PREFIX}/reservation`, reservationRouter)
app.use(`/${SERVER_API_PREFIX}/community`  , communityRouter)
app.use(`/${SERVER_API_PREFIX}/repair`     , repairRouter)
app.use(`/${SERVER_API_PREFIX}/preordain`  , preordainRouter)
app.use(`/${SERVER_API_PREFIX}/bill`       , billRouter)
app.use(`/${SERVER_API_PREFIX}/contract`   , contractRouter)

// 404请求
app.use((req, res) => {
  loger.info(`无法响应请求(404) ${req.method} ${req.path}`)
  res.status(404).send(new Result(null, CODE_ERROR, '404 NOT FIND'))
})

// 启动服务器
const server = SERVER_PROTOCOL === 'https' ? httpsServer : app
server.listen(SERVER_PORT, () => {
  const cost = new Date().getTime() - __start
  loger.info('服务器运行成功! 耗时: ' + cost + 'ms')
  console.log(`
  ______    __    _______       ________      _____ ______ _______      __ 
  |_   _|   | |   |_   _\\ \\    / /  ____|    / ____|  ____|  __ \\ \\    / / 
    | |     | |     | |  \\ \\  / /| |__      | (___ | |__  | |__) \\ \\  / /
    | |     | |     | |   \\ \\/ / |  __|      \\___ \\|  __| |  _  / \\ \\/ / 
   _| |_    | |___ _| |_   \\  /  | |____     ____) | |____| | \\ \\  \\  /   
  |_____|   |_____|_____|   \\/   |______|   |_____/|______|_|  \\_\\  \\/   
                                               CSJ@CSU Graduation Project`)
  console.log('  Protocol:         ' + SERVER_PROTOCOL)
  console.log('  IP:               ' + SERVER_ADDRESS)
  console.log('  Port:             ' + SERVER_PORT)
  console.log('  Credentials:      ' + SERVER_ALLOW_CREDENTIALS)
  console.log('  ENV:              ' + ENV)
  console.log('  Client url:       ' + getServerPath() + '/client')
  console.log('  PowerBy:          ' + 'Express 4.16.4')
  console.log('')
  schedule.start()
})

// process.on('uncaughtException', (err) => {
//   loger.info('Caught exception: ' + err.message)
// })