// env
module.exports.ENV = process.env.NODE_ENV // 当前环境 production生产 development开发
module.exports.DEVELOPMENT = process.env.NODE_ENV === 'development'
module.exports.PRODUCTION = process.env.NODE_ENV === 'production'

// mysql
module.exports.DB_HOST     = 'localhost' // 地址
module.exports.DB_USER     = 'root'      // 用户
module.exports.DB_PASSWORD = '123'       // 密码
module.exports.DB_DATABASE = 'ilive'     // 库

// server
module.exports.SERVER_PROTOCOL        = 'http'     // 协议
module.exports.SERVER_HTTPS_FILES_URL = './'       // https协议文件存放地址
module.exports.SERVER_STATIC_URL      = './public' // 静态资源存放点
module.exports.SERVER_API_PREFIX      = 'api'      // api前缀

// 地址
module.exports.SERVER_ADDRESS = process.env.NODE_ENV === 'production'
                              ? '39.108.107.156'
                              : '127.0.0.1'
// 端口
module.exports.SERVER_PORT = process.env.NODE_ENV === 'production'
                           ? 80
                           : 8088
// 允许跨域的地址
module.exports.SERVER_ALLOW_ORIGIN = process.env.NODE_ENV === 'production'
                                   ? 'http://39.108.107.156'
                                   : 'http://127.0.0.1:8888'
// 是否允许跨域交换cookie
module.exports.SERVER_ALLOW_CREDENTIALS = process.env.NODE_ENV === 'development'

// response code
module.exports.CODE_SUCCESS   = 0 // 成功
module.exports.CODE_ERROR     = 1 // 失败
module.exports.CODE_NOT_LOGIN = 2 // 没有登录

// cookie
module.exports.COOKIE_MAXAGE = 10 * 60 * 1000 // 10分钟过期时间 (ms)
module.exports.COOKIE_SECRET = 'ILIVEcsj2019' // session 签名

// log
module.exports.LOG_ECHO_SUCCESS = true  // 允许显示SUCCESS信息
module.exports.LOG_ECHO_DANGER  = true  // 允许显示DANGER信息
module.exports.LOG_ECHO_INFO    = true  // 允许保存INFO信息
module.exports.LOG_SAVE_SUCCESS = false // 允许保存SUCCESS信息
module.exports.LOG_SAVE_DANGER  = false // 允许保存DANGER信息
module.exports.LOG_SAVE_INFO    = false // 允许显示INFO信息
module.exports.LOG_DATABASE     = true  // 记录数据库请求
module.exports.LOG_REQUEST      = true  // 记录请求
module.exports.LOG_RESPONSE     = true  // 记录响应
module.exports.LOG_ENTITY       = false // 记录实体事件

// AMap
module.exports.AMAP_KEY_SUPPORT = '72ac171690b6e7e5ddda3bcfc42286fc'
module.exports.AMAP_KEY_JSSDK   = '7bd49f0a41b3c7b82bcb2327cdddee6d'

// 短信服务
module.exports.MAIL_ENABLE = false // process.env.NODE_ENV === 'production'

// 公司设定
module.exports.COMP_NAME    = '长沙邦布物业管理有限公司'
module.exports.COMP_ADDRESS = '长沙市天心区韶山南路68号'

// 计划任务
// 频率 0=每天1次 1=每小时1次 2=每分钟1次 3=每秒1次
module.exports.SCHEDULE_DURATION_CLEAR   = 2 // 清理频率
module.exports.SCHEDULE_DURATION_MESSAGE = 0 // 消息发送频率