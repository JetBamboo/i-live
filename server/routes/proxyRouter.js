const express = require('express')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config.js')
const proxyService = require('../service/proxyService')
const Loger = require('../utils/loger')

const router = express.Router()
const loger = new Loger('proxyRouter.js')

router.get('/address_tips', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const { city, keywords } = req.query
  try {
    result.value = await proxyService.getAddressTips(keywords, city)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router