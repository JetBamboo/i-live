const express = require('express')
const Loger = require('../utils/loger')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const Source   = require('../bean/Source')
const RoomType = require('../bean/RoomType')
const Room     = require('../bean/Room')

const userService      = require('../service/userService')
const billService      = require('../service/billService')
const sourceService    = require('../service/sourceService')
const roomService      = require('../service/roomService')
const preordainService = require('../service/preordainService')
const contractService  = require('../service/contractService')

const router = express.Router()
const loger = new Loger('billRouter.js')

router.get('/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    if (!userId) throw new Error('pin不合法')
    const billList = await billService.getListByUserId(userId)
    for (let i = 0; i < billList.length; i++) {
      const bill = billList[i]
      try {
        bill.source = await sourceService.getSourceById(bill.sourceId)
      } catch (err) {
        bill.source = new Source()
        console.warn(err)
      }
      try {
        bill.room = await roomService.getRoomById(bill.roomId)
      } catch (err) {
        bill.room = new Room()
        console.warn(err)
      }
      try {
        bill.roomType = await roomService.getRoomTypeById(bill.roomTypeId)
      } catch (err) {
        bill.roomType = new RoomType()
        console.warn(err)
      }
      bill.waterList   = await billService.getWaterList(bill.id)
      bill.elecList    = await billService.getElecList(bill.id)
      bill.rentList    = await billService.getRentList(bill.id)
      bill.depositList = await billService.getDepositList(bill.id)
    }
    result.value = billList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 通过账单ID获取账单
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = req.query.id
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    if (!userId) throw new Error('pin不合法')
    const bill = await billService.getById(id)
    try {
      bill.source = await sourceService.getSourceById(bill.sourceId)
    } catch (err) {
      bill.source = new Source()
      console.warn(err)
    }
    try {
      bill.room = await roomService.getRoomById(bill.roomId)
    } catch (err) {
      bill.room = new Room()
      console.warn(err)
    }
    try {
      bill.roomType = await roomService.getRoomTypeById(bill.roomTypeId)
    } catch (err) {
      bill.roomType = new RoomType()
      console.warn(err)
    }
    bill.waterList   = await billService.getWaterList(bill.id)
    bill.elecList    = await billService.getElecList(bill.id)
    bill.rentList    = await billService.getRentList(bill.id)
    bill.depositList = await billService.getDepositList(bill.id)
    result.value = bill
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 支付账单
router.put('/pay', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = req.body.id
    if (!id) throw new Error('ID不存在')
    await userService.getUserIdByPin(req.cookies.pin)
    const currentDate = new Date()
    const bill = await billService.getById(id)
    
    // 是预定金账单，查看其他同类型订单是否已经先行预定，将预定转为已预定
    // 防止高并发状态不同步
    if (bill.type === 0) {
      const preordain = await preordainService.getByBill(bill)
      const preordainList = await preordainService.getListByRoomId(bill.roomId)
      let pass = true
      for (let i = 0; i < preordainList.length; i++) {
        if (preordainList[i].status === 1 && preordain.id !== preordainList[i].id) { 
          pass = false
        }
      }
      if (!pass)
        throw new Error('该房间被其他人抢先预定了')
      // await preordainService.payDeposit(bill.roomId, bill.userId, bill.date)
      // 修改预定订单属性
      const dateEnd = new Date()
      dateEnd.setDate(currentDate.getDate() + 30)
      preordain.dateEnd = dateEnd.getTime()
      preordain.status = 1
      await preordainService.edit(preordain)

      // 修改房间属性
      const room = await roomService.getRoomById(preordain.roomId)
      if (room.status !== 0)
        throw new Error(`房间不可用 status=${room.status}`)
      await roomService.changeStatus(preordain.roomId, 1)
      await roomService.changeOwner(preordain.roomId, bill.userId)
    } 
    // 是房屋首期账单，则将合同转为待审核
    else if (bill.type === 1) {
      const contract = await contractService.getByBill(bill)
      await contractService.changeStatus(contract.id, 1)
    }
    // 如果是合同账单，则不做任何事情（没想好）
    else if (bill.type === 2) {
    }

    // 最后进行支付并改变状态
    try { await billService.changeWaterStatusByBill(id, 1)   } catch (err) { console.warn(err) }
    try { await billService.changeElecStatusByBill(id, 1)    } catch (err) { console.warn(err) }
    try { await billService.changeDepositStatusByBill(id, 1) } catch (err) { console.warn(err) }
    try { await billService.changeRentStatusByBill(id, 1)    } catch (err) { console.warn(err) }
    try { await billService.changeStatus(id, 1)              } catch (err) { console.warn(err) }
    
    result.value = {}
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router