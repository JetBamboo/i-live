const express = require('express')
const Loger = require('../utils/loger')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const Source      = require('../bean/Source')
const RoomType    = require('../bean/RoomType')
const Room        = require('../bean/Room')
const Contract    = require('../bean/Contract')
const Bill        = require('../bean/Bill')
const BillRent    = require('../bean/BillRent')
const BillDeposit = require('../bean/BillDeposit')
const ContractLog = require('../bean/ContractLog')

const userService      = require('../service/userService')
const billService      = require('../service/billService')
const sourceService    = require('../service/sourceService')
const roomService      = require('../service/roomService')
const preordainService = require('../service/preordainService')
const contractService  = require('../service/contractService')

const router = express.Router()
const loger = new Loger('contractRouter.js')

// 添加合同
router.post('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    let contract = new Contract(req.body)
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    const currentDate = new Date()
    contract.userId = userId
    contract.date = currentDate.getTime()
    contract.status = 0

    // 1.添加合同
    contract = await contractService.add(contract)
    console.log('~~~添加合同', contract)

    // 2.如果房间是预定中，则将对应预定变为已签约状态
    const room = await roomService.getRoomById(contract.roomId)
    if (room.status === 1) {
      const preordain = await preordainService.getEffectByRoomId(room.id)
      await preordainService.changeStatus(preordain.id, 2)
      console.log('~~~房间被预定', preordain)
    }
    

    // 3.房间变为租赁中状态
    await roomService.changeStatus(room.id, 2)
    console.log('~~~房间租赁状态改变', 2)

    // 4.1添加账单
    const roomType = await roomService.getRoomTypeById(room.typeId)
    let bill = new Bill({
      userId,
      roomId: room.id,
      roomTypeId: roomType.id,
      sourceId: room.sourceId,
      date: currentDate.getTime(),
      status: 0,
      type: 1
    })
    bill = await billService.add(bill)
    console.log('~~~添加订单', bill)

    // 4.2添加房租账单
    const rent = new BillRent({
      billId: bill.id,
      date: currentDate.getTime(),
      price: roomType.price * 3
    })
    await billService.addRent(rent)
    console.log('~~~添加订单', rent)

    // 4.3添加定金账单
    const deposit = new BillDeposit({
      billId: bill.id,
      date: currentDate.getTime(),
      price: roomType.price
    })
    await billService.addDeposit(deposit)
    console.log('~~~添加订单', deposit)

    result.value = contract
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 获取合同列表
router.get('/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    const contractList = await contractService.getListByUserId(userId)
    for (let i = 0; i < contractList.length; i++) {
      const contract = contractList[i]
      contract.source   = await sourceService.getSourceById(contract.sourceId)
      contract.room     = await roomService.getRoomById(contract.roomId)
      contract.roomType = await roomService.getRoomTypeById(contract.roomTypeId)
    }
    result.value = contractList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 获取合同
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = req.query.id
    if (!id) throw new Error('无法获取ID')
    await userService.getUserIdByPin(req.cookies.pin)
    const contract    = await contractService.getById(id)
    contract.source   = await sourceService.getSourceById(contract.sourceId)
    contract.room     = await roomService.getRoomById(contract.roomId)
    contract.roomType = await roomService.getRoomTypeById(contract.roomTypeId)
    result.value = contract
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 添加记录
router.post('/log', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    await getUserIdByPin(req.cookies.pin)
    let log = new ContractLog(req.body)
    log = await contractService.addLog(log)
    result.value = log
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router