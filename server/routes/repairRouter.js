const express = require('express')
const Loger = require('../utils/loger')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const Result   = require('../bean/Result')
const Source   = require('../bean/Source')
const Repair   = require('../bean/Repair')
const RoomType = require('../bean/RoomType')
const Room     = require('../bean/Room')

const sourceService = require('../service/sourceService')
const repairService = require('../service/repairService')
const userService   = require('../service/userService')
const roomService   = require('../service/roomService')

const router = express.Router()
const loger = new Loger('repairRouter.js')

// 获取报修列表
router.get('/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.cookies.pin
  try {
    if (!id) throw new Error('无法获取用户PIN')
    const repairList = await repairService.getRepairList(id)
    for (let i = 0; i < repairList.length; i++) {
      const repair = repairList[i]
      try {
        repair.source = await sourceService.getSourceById(repair.sourceId)
      } catch (err) {
        repair.source = new Source()
        console.warn(err)
      }
      try {
        repair.roomType = await roomService.getRoomTypeById(repair.roomTypeId)
      } catch (err) {
        repair.roomType = new RoomType()
        console.warn(err)
      }
      try {
        repair.room = await roomService.getRoomById(repair.roomId)
      } catch (err) {
        repair.room = new Room()
        console.warn(err)
      }
    }
    result.value = repairList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据ID获取报修详情
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = req.query.id
    if (!id) throw new Error('无法获取ID')
    await userService.getUserIdByPin(req.cookies.pin)
    const repair = await repairService.getById(id)
    try {
      repair.source = await sourceService.getSourceById(repair.sourceId)
    } catch (err) {
      repair.source = new Source()
      console.warn(err)
    }
    try {
      repair.roomType = await roomService.getRoomTypeById(repair.roomTypeId)
    } catch (err) {
      repair.roomType = new RoomType()
      console.warn(err)
    }
    try {
      repair.room = await roomService.getRoomById(repair.roomId)
    } catch (err) {
      repair.room = new Room()
      console.warn(err)
    }
    result.value = repair
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 新怎报修
router.post('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    let repair = new Repair(req.body)
    repair.date = new Date().getTime()
    repair.status = 0
    repair.userId = userId
    repair = await repairService.add(repair)
    result.value = repair
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router