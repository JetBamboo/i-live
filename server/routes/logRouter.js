const express = require('express')
const request = require('request')
const Result = require('../bean/Result')

const Log = require('../bean/Log')

const logService = require('../service/logService')

const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const router = express.Router()

router.post('/', (req, res) => {
  logService.log(new Log(req.body)).then((result) => {
    res.send(new Result('', CODE_SUCCESS, result))
  }).catch((err) => {
    res.send(new Result('', CODE_ERROR, err.message))
  })
})