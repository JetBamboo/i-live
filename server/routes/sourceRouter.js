const express = require('express')
const sourceService = require('../service/sourceService')
const roomService = require('../service/roomService')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')
const Loger = require('../utils/loger')

// const crypto = require('../utils/crypto')
const router = express.Router()
const loger = new Loger('sourceRouter.js')

// 获取房源信息
router.get('/city', async (req, res) => {
  loger.request(req)
  const city = req.body.city
  const result = new Result()
  try {
    result.value = await sourceService.getSourceListByCity(city)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据城市获取推荐房源
router.get('/recommend', async (req, res) => {
  loger.request(req)
  const city = req.query.city
  const result = new Result()
  try {
    const sourceList = await sourceService.getSourceListByCity(city)
    const length = sourceList.length
    for (let i = 0; i < length; i++) {
      const source = sourceList[i]
      const id = source.id
      source.roomTypeList = await roomService.getRoomTypeListBySourceId(id)
      source.roomList = await roomService.getRoomListBySourceId(id)
    }
    result.value = sourceList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    throw err
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据ID获取房源
router.get('/', async (req, res) => {
  loger.request(req)
  const id = req.query.id || 0
  const result = new Result()
  try {
    const source = await sourceService.getSourceById(id)
    try {
      source.roomTypeList = await roomService.getRoomTypeListBySourceId(id)
    } catch (err) {
      console.warn(err)
    }
    const priceSet = source.roomTypeList.map(r => r.price)
    source.priceMin = Math.min(...priceSet)
    source.priceMax = Math.max(...priceSet)
    result.value = source
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据所在城市获取房源
router.get('/list', async (req, res) => {
  loger.request(req)
  const city = req.query.city
  const result = new Result()
  try {
    const sourceList = await sourceService.getSourceListByCity(city)
    const length = sourceList.length
    for (let i = 0; i < length; i++) {
      const source = sourceList[i]
      const id = source.id
      source.roomTypeList = await roomService.getRoomTypeListBySourceId(id)
      source.roomList = await roomService.getRoomListBySourceId(id)
    }
    result.value = sourceList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    throw err
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.get('/list/name', async (req, res) => {
  loger.request(req)
  const name = req.query.name
  const result = new Result()
  try {
    const sourceList = await sourceService.getListByName(name)
    const length = sourceList.length
    for (let i = 0; i < length; i++) {
      const source = sourceList[i]
      const id = source.id
      source.roomTypeList = await roomService.getRoomTypeListBySourceId(id)
      source.roomList = await roomService.getRoomListBySourceId(id)
    }
    result.value = sourceList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    throw err
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router
