const express = require('express')
const request = require('request')
const Result = require('../bean/Result')

const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const router = express.Router()

router.get('/phone', (req, res) => {
  request({
    method:'post',
    url: 'https://sms.yunpian.com/v2/sms/single_send.json',
    headers: {
      'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
    },
    form:{
      apikey: 'dd214895c1b99515389251fd4175c4d3',
      mobile: '15111012876',
      text: '【云片网】您的验证码是123456',
    }
  }, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      res.send(new Result('123456', CODE_SUCCESS, body))
    } else if (!error && response.statusCode !== 200) {
      res.send(new Result('', CODE_ERROR, body))
    } else {
      res.send(new Result('', CODE_ERROR, error))
    }
  })
})

router.get('/addSession', (req) => {
  req.session.test = 'test'
})

router.get('/getSession', (req) => {
  console.log(req.session.test)
})

router.get('/delSession', (req) => {
  req.session.test = undefined
})

router.post('/session', (req, res) => {
  if (req.session.user) {
    console.log('session ' + req.session.user.name + ' 存在')
  } else {
    console.log('session user 不存在，即将添加')
    req.session.user = {}
    req.session.user.name = 'chaos'
  }
  req.session.save()
  res.send(req.session)
})

router.get('/empty', (req, res) => {
  res.send({})
})


module.exports = router