const express = require('express')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')
const Loger = require('../utils/loger')
const Reservation = require('../bean/Reservation')

const userService = require('../service/userService')
const roomService = require('../service/roomService')
const reservationService = require('../service/reservationService')
const sourceService = require('../service/sourceService')

const router = express.Router()
const loger = new Loger('reservationRouter.js')

// 添加预约
router.post('/add', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const form = req.body
  form.userId = req.cookies.pin
  try {
    if (!form.userId) throw new Error('无法获取用户pin')
    form.sourceId = +form.sourceId
    form.userId = +form.userId
    form.date = new Date().getTime()
    const reservation = new Reservation(form)
    result.value = await reservationService.addReservation(reservation)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据用户ID获取预约
router.get('/list', async (req, res) => {
  loger.request(req)
  const id = req.cookies.pin
  const result = new Result()
  try {
    if (!id) throw new Error('无法获取用户pin')
    const reservationList = await reservationService.getReservationListByUserId(id)
    for (let i = 0; i < reservationList.length; i++) {
      try {
        reservationList[i].source = await sourceService.getSourceById(reservationList[i].sourceId)
      } catch (err) {
        reservationList[i].source = {}
        console.warn(err)
      }
    }
    // 以ID倒序排列，最新添加的放最上面
    reservationList.sort((a, b) => b.id - a.id)
    result.value = reservationList 
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据预约ID获取预约信息
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.query.id
  try {
    if (!id) throw new Error('错误的预约ID')
    const reservation = await reservationService.getReservationById(id)
    try {
      reservation.source = await sourceService.getSourceById(reservation.sourceId)
    } catch (err) {
      reservation.source = {}
      console.warn(err)
    }
    try {
      reservation.roomType = await roomService.getRoomTypeById(reservation.typeId)
    } catch (err) {
      reservation.roomType = {}
      console.warn(err)
    }
    result.code = CODE_SUCCESS
    result.value = reservation
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据预约ID取消预约
router.delete('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.query.id
  try {
    if (!id)
      throw new Error('错误的预约ID')
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    const reservation = await reservationService.getReservationById(id)
    if (userId !== reservation.userId)
      throw new Error('无权操作')
    reservation.status = 3
    await reservationService.editReservation(reservation)
    result.code = CODE_SUCCESS
    result.value = reservation
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router
