const express = require('express')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')
const Loger = require('../utils/loger')
const numberUtils = require('../utils/numberUtils')
const Result = require('../bean/Result')
const Source = require('../bean/Source')
const RoomType = require('../bean/RoomType')

const userService = require('../service/userService')
const roomService = require('../service/roomService')
const sourceService = require('../service/sourceService')
const commentService = require('../service/commentService')

const router = express.Router()
const loger = new Loger('roomRouter.js')

router.get('/type', async (req, res) => {
  loger.request(req)
  const id = req.query.id
  const result = new Result()
  try {
    const roomType = await roomService.getRoomTypeById(id)
    roomType.source = await sourceService.getSourceById(roomType.sourceId)
    result.value = roomType
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.get('/type_list', async (req, res) => {
  loger.request(req)
  const id = req.query.id
  const result = new Result()
  try {
    result.value = await roomService.getRoomTypeListBySourceId(id)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})
   
router.get('/type_display_list', async (req, res) => {
  loger.request(req)
  const id = req.query.id
  const result = new Result()
  try {
    result.value = await roomService.getRoomTypeDisplayListBySourceId(id)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.get('/recommend', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const city = req.query.city
  try {
    if (!city) throw new Error('没有城市参数')
    const sourceList = await sourceService.getSourceListByCity(city)
    // 获取房源星级
    for (let i = 0; i < sourceList.length; i++) {
      const source = sourceList[i]
      const comments = await commentService.getCommentsBySourceId(source.id)
      source.rate = numberUtils.getAve(comments, 'rate')
    }
    // 将房源用星级进行排序
    sourceList.sort((a, b) => a.rate - b.rate)
    result.value = sourceList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    res.send(result)
    loger.response(result)
  }
})

// 根据用户ID获取房间
router.get('/list/user', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = await userService.getUserIdByPin(req.cookies.pin)
    const roomList = await roomService.getRoomListByUserId(id)
    for (let i = 0; i < roomList.length; i++) {
      try {
        roomList[i].source = await sourceService.getSourceById(roomList[i].id)
      } catch (err) {
        console.warn(err.message)
        roomList[i].source = new Source()
      }
    }
    result.value = roomList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.get('/list/safe', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const { id, code } = req.query
    await userService.getUserIdByPin(req.cookies.pin)
    const roomList = await roomService.getListSafe(id, code)
    for (let i = 0; i < roomList.length; i++) {
      try {
        roomList[i].type = await roomService.getRoomTypeById(roomList[i].typeId)
      } catch (err) {
        roomList[i].type = new RoomType()
        console.warn(err.message)
      }
    }
    result.value = roomList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.get('/type/recommend', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const city = req.query.city
    const sourceList = await sourceService.getSourceListByCity(city)
    const roomList = []
    for (let i = 0; i < sourceList.length; i++) {
      const roomTypeList = await roomService.getRoomTypeListBySourceId(sourceList[i].id)
      const roomType = roomTypeList[0]
      roomType.source = sourceList[i]
      roomList.push(roomType)
    }
    result.value = roomList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据用户ID获取用户正在租赁的房间
router.get('/list/user_using', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = await userService.getUserIdByPin(req.cookies.pin)
    const roomList = await roomService.getRoomListByUserId(id)
    const resultList = []
    for (let i = 0; i < roomList.length; i++) {
      if (roomList[i].ownerId !== id || roomList[i].status !== 2) continue
      try {
        roomList[i].source = await sourceService.getSourceById(roomList[i].id)
      } catch (err) {
        console.warn(err.message)
        roomList[i].source = new Source()
      } finally {
        resultList.push(roomList[i])
      }
    }
    result.value = resultList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})
module.exports = router
