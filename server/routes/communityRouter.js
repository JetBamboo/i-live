const express = require('express')
const Loger = require('../utils/loger')
const Result = require('../bean/Result')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')
const communityService = require('../service/communityService')
const sourceService = require('../service/sourceService')
const userService = require('../service/userService')
const ActivityParticipate = require('../bean/ActivityParticipate')

const router = express.Router()
const loger = new Loger('communityRouter.js')

// 获取文章列表
router.get('/article/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    result.value = await communityService.getArticleList()
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据文章ID获取文章
router.get('/article', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.query.id
  try {
    const article = await communityService.getArticleById(id)
    article.source = await sourceService.getSourceById(article.sourceId)
    result.value = article
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 获取活动表
router.get('/activity/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    result.value = await communityService.getActivityList()
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据文章ID获取文章
router.get('/activity', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.query.id 
  try {
    const activity = await communityService.getActivityById(id)
    activity.source = await sourceService.getSourceById(activity.sourceId)
    const apList = await communityService.getAPListByActivityId(id)
    for (let i = 0; i < apList.length; i++) {
      try {
        apList[i].user = await userService.getUserById(apList[i].userId)
      } catch (err) {
        apList[i].user = {}
        console.warn(err)
      }
    }
    activity.participates = apList
    result.value = activity
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.post('/activity/join', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const activityId = +req.body.id
  const userId = +req.cookies.pin
  try {
    if (!userId)
      throw new Error('用户pin不存在')
    const ap = await communityService.getAPByUserAndActivityId(userId, activityId)
    // 如果预约不存在,则添加一条预约
    if (ap.id === -1)
      result.value = await communityService.addActivityParticipate(ap)
    // 如果预约存在但是处于未预约状态(status=2),则预约
    else if (ap.status === 2) {
      await communityService.joinActivity(ap)
      ap.status = 0
      result.value = ap
    }
    // 如果预约存在且已经预约(status=0),则不予预约
    else if (ap.status === 0)
      throw new Error('已经参与该活动了')
    // 如果活动已结束(status=1),则不予预约
    else if (ap.status === 1)
      throw new Error('活动已结束')
    // 如果预约存在且不是以上情况的话,不予预约
    else
      throw new Error('未知错误,禁止预约')
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

router.put('/activity/quit', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const activityId = +req.body.id
  const userId = +req.cookies.pin
  try {
    if (!userId)
      throw new Error('用户pin不存在')
    const ap = await communityService.getAPByUserAndActivityId(userId, activityId)
    // 如果不存在记录(id=-1)则不允许取消报名
    if (ap.id === -1)
      throw new Error('未参与活动')
    // 如果记录存在且处于未参与(status!=0)则不允许取消报名
    else if (ap.status !== 0)
      throw new Error('未参与活动')
    // 其他情况,允许取消报名
    else
      await communityService.quitActivity(ap)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router
