const express = require('express')
const userService = require('../service/userService')
const externalService = require('../service/externalService')
const Result = require('../bean/Result')
const User = require('../bean/User')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')
const Loger = require('../utils/loger')
const UserAuth = require('../bean/UserAuth')

// const crypto = require('../utils/crypto')
const router = express.Router()
const loger = new Loger('userRouter.js')

// 获取用户列表
router.get('/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const userList = await userService.getUserList()
    for (let i = 0; i < userList.length; i++) {
      userList[i].auth = new UserAuth()
    }
    result.value = userList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据ID获取用户
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.query.id
  try {
    const user = await userService.getUserById(id)
    user.auth = new UserAuth()
    result.value = user
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 根据cookie里的id获取用户
router.get('/pin', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.cookies.pin
  try {
    if (!id) throw new Error('pin不存在')
    const user = await userService.getUserById(id)
    user.auth = await userService.getAuthById(id)
    result.value = user
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 更新用户数据
router.put('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const user = new User(req.body.user)
  try {
    await userService.editUser(user)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 更新用户头像
router.put('/avatar', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const id = req.cookies.pin
  const url = req.body.url
  try {
    if (!id) throw new Error('pin not exist!')
    await userService.changeUserAvatar(id, url)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 获取验证码
router.get('/verify_code', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const phone = req.query.phone
  try {
    const code = await externalService.sendMailVerifyCode(phone)
    loger.info(`<userRouter.js> 获取验证码 ${phone} ${code}`)
    res.cookie('verifyCode', code, { maxAge: 10 * 60 * 1000, httpOnly: true })
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 用户登录
router.post('/login', async (req, res) => {
  loger.request(req)
  const result = new Result()
  const phone = req.body.phone
  const tempUser = new User({
    phone,
    username: phone,
    name: phone,
    dateCreate: new Date().getTime()
  })
  try {
    if (req.cookies.verifyCode !== req.body.code)
      throw new Error('验证码错误')
    // 看用户是否存在于数据库中(依据: username)
    // 存在:   获取已存在用户ID
    // 不存在: 添加用户, 获取用户ID
    let user = null
    try {
      user = await userService.getUserByUsername(tempUser.username)
    } catch (err) {
      user = await userService.addUser(tempUser)
      console.warn(err)
    }
    const id = user.id

    // 获得认证信息
    try {
      user.auth = await userService.getAuthById(id)
    } catch (err) {
      const auth = new UserAuth({ id, status: 0 })
      user.auth = await userService.addAuth(auth)
      console.warn(err)
    }

    loger.info(`用户登录 ID: ${id}`)

    // 清除用户浏览器种的验证cookie, 同时添加验证是否登录的pin(相当于用户ID)
    // cookie过期时间: 24小时, 不允许前端访问
    res.clearCookie('verifyCode')
    res.cookie('pin', id, { maxAge: 24 * 60 * 60 * 1000, httpOnly: false })
    result.value = user
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 用户是否登录
router.get('/isLogin', (req, res) => {
  loger.request(req)
  const result = !!req.cookies.pin
    ? new Result(true, CODE_SUCCESS, '用户已登录')
    : new Result(false, CODE_ERROR, '用户未登录')
  loger.response(req, result)
  res.send(result)
})

// 用户登出
router.get('/logout', (req, res) => {
  loger.request(req)
  let result = null
  if (req.cookies.pin) {
    loger.info(`用户退出登录 ID: ${req.cookies.pin}`)
    res.clearCookie('pin')
    result = new Result(true, CODE_SUCCESS, '登出成功')
  } else {
    result = new Result(false, CODE_ERROR, '用户未登录')
  }
  loger.response(req, result)
  res.send(result)
})

// 管理员用户登录
router.post('/login/root', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    // 看用户是否存在于数据库中(依据: username)
    // 存在:   获取已存在用户ID
    // 不存在: 添加用户, 获取用户ID
    const user = await userService.getUserByUsername('root')
    const id = user.id

    // 获得认证信息
    user.auth = await userService.getAuthById(id)

    loger.info(`Root用户登录 ID: ${id}`)

    // 清除用户浏览器种的验证cookie, 同时添加验证是否登录的pin(相当于用户ID)
    // cookie过期时间: 24小时, 不允许前端访问
    res.clearCookie('verifyCode')
    res.cookie('pin', id, { maxAge: 24 * 60 * 60 * 1000, httpOnly: false })
    result.value = user
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 修改认证信息
router.put('/auth', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = await userService.getUserIdByPin(req.cookies.pin)
    const auth = await userService.getAuthById(id)
    const tempAuth = new UserAuth(req.body.form)
    auth.name = tempAuth.name
    auth.card = tempAuth.card
    auth.cardType = tempAuth.cardType
    auth.cardFront = tempAuth.cardFront
    auth.cardBack = tempAuth.cardBack
    auth.status = 2 // 修改为认证中
    result.value = await userService.editAuth(auth)
    result.code = CODE_SUCCESS
  } catch (err) {
    result.desc = err.message
    result.code = CODE_ERROR
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router
