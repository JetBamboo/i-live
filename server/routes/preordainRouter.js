const express = require('express')
const Loger = require('../utils/loger')
const { CODE_SUCCESS, CODE_ERROR } = require('../config')

const Result      = require('../bean/Result')
const Preordain   = require('../bean/Preordain')
const Source      = require('../bean/Source')
const Room        = require('../bean/Room')
const RoomType    = require('../bean/RoomType')
const Bill        = require('../bean/Bill')
const BillDeposit = require('../bean/BillDeposit')

const preordainService = require('../service/preordainService')
const sourceService    = require('../service/sourceService')
const userService      = require('../service/userService')
const roomService      = require('../service/roomService')
const billService      = require('../service/billService')

const router = express.Router()
const loger = new Loger('preordainRouter.js')

// 获取预订表
router.get('/list', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = await userService.getUserIdByPin(req.cookies.pin)
    const preordainList = await preordainService.getListByUserId(id)
    for (let i = 0; i < preordainList.length; i++) {
      const preordain = preordainList[i]
      try {
        preordain.source = await sourceService.getSourceById(preordain.sourceId)
      } catch (err) {
        preordain.source = new Source()
        console.warn(err)
      }
      try {
        preordain.room = await roomService.getRoomById(preordain.roomId)
      } catch (err) {
        preordain.room = new Room()
        console.warn(err)
      }
      try {
        preordain.roomType = await roomService.getRoomTypeById(preordain.roomTypeId)
      } catch (err) {
        preordain.roomType = new RoomType()
        console.warn(err)
      }
    }
    result.value = preordainList
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 添加预定
router.post('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    let preordain = new Preordain(req.body)
    const userId = await userService.getUserIdByPin(req.cookies.pin)
    preordain.userId = userId

    const currentDate = new Date()

    // 添加定金账单
    let bill = new Bill({
      userId,
      roomId: preordain.roomId,
      sourceId: preordain.sourceId,
      roomTypeId: preordain.roomTypeId,
      date: currentDate.getTime(),
    })
    bill = await billService.add(bill)

    // 添加定金账单
    const source = await sourceService.getSourceById(preordain.sourceId)
    const deposit = new BillDeposit({
      billId: bill.id,
      date: currentDate.getTime(),
      price: source.deposit,
      status: 0
    })
    await billService.addDeposit(deposit)

    // 对创建日期进行设置
    preordain.date = currentDate.getTime()

    // 持久化数据
    preordain = await preordainService.add(preordain)
    result.value = preordain
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

// 获得预定详情
router.get('/', async (req, res) => {
  loger.request(req)
  const result = new Result()
  try {
    const id = req.query.id
    if (!id) throw new Error('预定ID不合法')
    await userService.getUserIdByPin(req.cookies.pin)
    const preordain = await preordainService.getById(id)
    try {
      preordain.source = await sourceService.getSourceById(preordain.sourceId)
    } catch (err) {
      preordain.source = new Source()
      console.warn(err)
    }
    try {
      preordain.room = await roomService.getRoomById(preordain.roomId)
    } catch (err) {
      preordain.room = new Room()
      console.warn(err)
    }
    try {
      preordain.roomType = await roomService.getRoomTypeById(preordain.roomTypeId)
    } catch (err) {
      preordain.roomType = new RoomType()
      console.warn(err)
    }
    result.value = preordain
    result.code = CODE_SUCCESS
  } catch (err) {
    result.code = CODE_ERROR
    result.desc = err.message
    console.error(err)
  } finally {
    loger.response(req, result)
    res.send(result)
  }
})

module.exports = router