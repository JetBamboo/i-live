const express = require('express')
const Result = require('../bean/Result')
const { CODE_ERROR } = require('../config')
const Loger = require('../utils/loger')
const path = require('path')
const fecha = require('fecha')
const { getServerPath } = require('../utils/stringUtils')

const router = express.Router()
const loger = new Loger('uploadRouter.js')

router.post('/file', (req, res) => {
  res.send(new Result({}, CODE_ERROR, 'Not implement'))
})

router.post('/image', (req, res) => {
  loger.request(req)
  const pin = req.cookies.pin || 'unknown'
  const avatar = req.files.image
  const filetype = avatar.name.split('.').pop()
  const date = fecha.format(new Date(), 'YYYY-MM-DD')
  const filename = `${pin}-${new Date().getTime() / 1000 | 0}.${filetype}`
  const result = new Result()
  avatar.mv(path.resolve(__dirname, `../public/upload/${date}/${filename}`)  , err => {
    if (err) {
      result.code = CODE_ERROR
      result.desc = err
      res.status(500).send(result)
    } else {
      const netname = `${getServerPath()}/upload/${date}/${filename}`
      loger.info('产生新文件 ' + netname)
      result.value = netname
      res.send(result)
    }
    loger.response(req, result)
  })
})

module.exports = router