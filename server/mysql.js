const mySql = require('mysql')
const config = require('./config')
const Loger = require('./utils/loger')

const loger = new Loger('mysql.js')

const pool = mySql.createPool({
  host: config.DB_HOST,
  user: config.DB_USER,
  password: config.DB_PASSWORD,
  database: config.DB_DATABASE
})

pool.on('acquire', connection => {
  // loger.info(`分配链接 ID:${connection.threadId}`)
})

pool.on('release', connection => {
  // loger.info(`释放链接 ID:${connection.threadId}`)
})

module.exports = (sql) => new Promise((resolve) => {
  // 从连接池获取连接
  pool.getConnection((connectionError, connection) => {
    if (connectionError) {
      throw connectionError
    }
    // 使用连接执行query
    loger.info(`执行sql ${sql}`)
    connection.query(sql, (queryError, result) => {
      if (queryError) {
        throw queryError
      }
      loger.sql(sql, result)
      resolve(result)
      connection.release()
    })
  })
})