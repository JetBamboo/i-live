const reservationService = require('../service/reservationService')
const Loger = require('../utils/loger')

const loger = new Loger('reservationSchedule.js')

// 查询当前日期是否超过预约的预约时间
// 如果超过并且status=0，则status=2
const changeStatus = async () => {
  loger.info('正在清理逾期预约')
  let count = 0
  try {
    const list = await reservationService.getListAll()
    const currentTime = new Date().getTime()
    for (let i = 0; i < list.length; i++) {
      const item = list[i]
      if (currentTime > item.dateStart && item.status === 1) {
        try {
          loger.info(`预约(id=${item.id}) 未赴约: SET status=2`)
          await reservationService.changeStatus(item.id, 2)
          count++
        } catch (err) {
          console.warn(err)
        }
      }
    }
  } catch (err) {
    console.error(err)
  } finally {
    loger.info('共清理了 ' + count + ' 条逾期预约')
  }
}

module.exports = () => {
  changeStatus()
}