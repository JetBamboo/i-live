const NodeSchedule = require('node-schedule')
const Loger = require('./utils/loger')
const {
  SCHEDULE_DURATION_CLEAR,
  SCHEDULE_DURATION_MESSAGE
} = require('./config')

const reservationSchedule = require('./schedules/reservationSchedule')

const loger = new Loger('schedule.js')

const rules = [
  new NodeSchedule.RecurrenceRule(),
  new NodeSchedule.RecurrenceRule()
]

const schedules = [null, null]

const PER_DAY    = [0] // 每天执行一次
const PER_HOUR   = [0] // 每小时执行一次
const PER_MINUTE = [0] // 每分钟执行一次
const PER_SECOND = Array.from({ length: 60 }, (_, i) => i) // 每秒执行一次

const CLEAR   = 0
const MESSAGE = 1

const durationSetter = (type) => [
  () => { rules[type].date   = PER_DAY    },
  () => { rules[type].minute = PER_HOUR   },
  () => { rules[type].second = PER_MINUTE },
  () => { rules[type].second = PER_SECOND }
]

durationSetter(CLEAR)[SCHEDULE_DURATION_CLEAR]()
durationSetter(MESSAGE)[SCHEDULE_DURATION_MESSAGE]()

// 启动计划
const start = () => {
  loger.info('计划任务启动')
  schedules[CLEAR] = NodeSchedule.scheduleJob(rules[CLEAR], () => {
    reservationSchedule()
  })
}

// 取消计划
const end = () => {
  loger.info('计划任务关闭')
  schedules.forEach(schedule => {
    schedule && schedule.cancel()
    schedule = null
  })
}

module.exports = {
  start,
  end
}