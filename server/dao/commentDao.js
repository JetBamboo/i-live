const query = require('../mysql')
const Comment = require('../bean/Comment')
const daoUtils = require('../utils/daoUtils')

/**
 * 通过房源ID获取评论
 * @param {String|Number} id 
 * @returns {Promise}
 */
const getCommentsBySourceId = id => new Promise(async resolve => {
  const results = await query(`SELECT * FROM comment WHERE source_id = ${id}`)
  const comments = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Comment(data)
  })
  resolve(comments)
})

module.exports = {
  getCommentsBySourceId
}
