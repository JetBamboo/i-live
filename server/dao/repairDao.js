const query = require('../mysql')
const daoUtils = require('../utils/daoUtils')
const Repair = require('../bean/Repair')

/**
 * 通过用户ID获取维修表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Repair>>}
 */
const getRepairList = id => new Promise(async resolve => {
  const results = await query(`SELECT * FROM repair WHERE user_id = ${id}`)
  const repairList = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Repair(data)
  })
  resolve(repairList)
})

// 添加
const add = repair => new Promise(async resolve => {
  const values = daoUtils.mapValues(repair, [
    'userId', 'roomId', 'sourceId', 'roomTypeId', 'title', 'content',
    'area', 'date', 'dateStart', 'picture', 'status', 'access'
  ])
  const results = await query('INSERT INTO repair ' + values)
  repair.id = results.insertId
  resolve(repair)
})

// 用ID获取详情
const getById = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM repair WHERE id = ${id}`)
  if (results.length === 0) {
    return reject(new Error('通过ID获取维修详情'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Repair(data))
})

module.exports = {
  getRepairList,
  add,
  getById
}