const query = require('../mysql')
const daoUtils = require('../utils/daoUtils')
const Preordain = require('../bean/Preordain')

/**
 * 通过用户ID获取预约列表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Preordain>>}
 */
const getListByUserId = id => new Promise(async resolve => {
  const results = await query(`SELECT * FROM preordain WHERE user_id = ${id}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Preordain(data)
  })
  resolve(list)
})

/**
 * 添加新的预定信息
 * @param {Preordain} preordain 预定实体
 * @returns {Promise<Preordain>}
 */
const add = preordain => new Promise(async (resolve, reject) => {
  const values = daoUtils.mapValues(preordain, [
    'sourceId', 'userId', 'roomId', 'roomTypeId', 'name',
    'phone', 'card', 'date', 'dateEnd', 'content'
  ])
  const results = await query('INSERT INTO preordain ' + values)
  preordain.id = results.insertId
  resolve(preordain)
})

/**
 * 通过ID获取预定
 * @param {Number|String} id 预定ID
 * @returns {Promise<Preordain>}
 */
const getById = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM preordain WHERE id = ${id}`)
  if (results.length === 0) {
    return reject(new Error('找不到预定信息'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Preordain(data))
})

/**
 * 根据房间ID，用户ID，时间戳寻找支付定金的预定
 * @deprecated
 * @param {Number|String} roomId 房间ID
 * @param {Number|String} userId 用户ID
 * @param {Number|String} date 创建时间
 * @returns {Promise<void>}
 */
const payDeposit = (roomId, userId, date) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE preordain SET status = 1 WHERE room_id = ${roomId} AND user_id = ${userId} AND date = ${date}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到预定'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 通过房间ID获取status=1的预定信息
 * @param {Number|String} roomId 房间ID
 * @returns {Promise<Preordain>}
 */
const getEffectByRoomId = roomId => new Promise(async (resolve, reject) => {
  const results = await query('SELECT * FROM preordain WHERE status = 1 AND room_id = ' + roomId)
  if (results.length === 0) {
    return reject(new Error('找不到指定预定信息'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Preordain(data))
})

/**
 * 改变状态
 * @param {Number|String} id 预定ID
 * @param {Number|String} status 状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE preordain SET status = ${status} WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到预定'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

// 通过账单获取预定
const getByBill = bill => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM preordain WHERE date = ${bill.date}`)
  if (results.length === 0) {
    return reject(new Error('找不到预定'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Preordain(data))
})

// 修改
const edit = preordain => new Promise(async (resolve, reject) => {
  const setters = daoUtils.mapSetters(preordain, [
    'sourceId', 'userId', 'roomId', 'roomTypeId', 'name',
    'phone', 'card', 'date', 'dateEnd', 'content', 'status'
  ])
  const results = await query('UPDATE preordain SET ' + setters + ' WHERE id = ' + preordain.id)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到预定信息'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

// 通过房间ID获取预定
const getListByRoomId = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM preordain WHERE room_id = ${id}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Preordain(data)
  })
  resolve(list)
})

module.exports = {
  getListByUserId,
  add,
  getById,
  payDeposit,
  getEffectByRoomId,
  changeStatus,
  getByBill,
  edit,
  getListByRoomId
}
