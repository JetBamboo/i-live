const query = require('../mysql')
const daoUtils = require('../utils/daoUtils')
const Activity = require('../bean/Activity')
const Article = require('../bean/Article')
const ActivityParticipate = require('../bean/ActivityParticipate')

/**
 * 获取文章
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getArticles = where => new Promise(resolve => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM article' + whereStr).then(results => {
    const items = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new Article(data)
    })
    resolve(items)
  })
})

/**
 * 获取文章列表
 * @returns {Promise}
 */
const getArticleList = _ => new Promise(async resolve => {
  _getArticles().then(resolve)
})

/**
 * 根据文章ID获取文章
 * @param {String|Number} id 文章ID 
 * @returns {Promise}
 */
const getArticleById = id => new Promise(async resolve => {
  _getArticles('id = ' + id).then(items => resolve(items[0]))
})

/**
 * 获取活动
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getActivities = where => new Promise(resolve => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM activity' + whereStr).then(results => {
    const items = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new Activity(data)
    })
    resolve(items)
  })
})

/**
 * 获取活动列表
 * @returns {Promise}
 */
const getActivityList = _ => new Promise(async resolve => {
  _getActivities().then(resolve)
})

/**
 * 根据活动ID获取活动
 * @param {String|Number} id 文章ID
 */
const getActivityById = id => new Promise(async resolve => {
  _getActivities('id =' + id).then(items => resolve(items[0]))
})

/**
 * 添加新的活动预约
 * @param {ActivityParticipate} ap 活动预约对象
 */
const addActivityParticipate = ap => new Promise(async resolve => {
  const values = daoUtils.mapValues(ap, ['userId', 'activityId', 'date'])
  query('INSERT INTO activity_participate ' + values)
    .then(results => {
      ap.id = results.insertId
      resolve(ap)
    })
})

/**
 * 通过ID来参加活动
 * @param {ActivityParticipate} ap 活动预约对象
 */
const joinActivity = ap => new Promise(async resolve => {
  const results = await query(`UPDATE activity_participate SET status = 0 WHERE id = ${ap.id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到活动预约对象'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 通过ID来取消活动参与
 * @param {ActivityParticipate} ap 活动预约对象
 */
const quitActivity = ap => new Promise(async resolve => {
  const results = await query(`UPDATE activity_participate SET status = 2 WHERE id = ${ap.id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到活动预约对象'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 获取活动预约数据
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getActivityParticipates = where => new Promise(resolve => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM activity_participate' + whereStr).then(results => {
    const items = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new ActivityParticipate(data)
    })
    resolve(items)
  })
})

/**
 * 通过活动ID获取活动预约表
 * @param {String|Number} id 
 * @returns {Promise}
 */
const getAPListByActivityId = id => new Promise(async resolve => {
  _getActivityParticipates('activity_id = ' + id).then(resolve)
})

/**
 * 通过用户ID与活动ID来获取活动预约
 * @param {Number|String} userId 用户ID
 * @param {Number|String} activityId 活动ID
 */
const getAPByUserAndActivityId = (userId, activityId) => new Promise(async resolve => {
  const results = await _getActivityParticipates([
    'activity_id = ' + activityId,
    'user_id = ' + userId
  ])
  const ap = new ActivityParticipate(results.length > 0 ? results[0] : {
    userId, activityId
  })
  resolve(ap)
})

/**
 * 是否已经参与了活动
 * @param {ActivityParticipate} ap 活动参与实体
 * @returns {Promise}
 */
const isAlreadyParticipate = ap => new Promise(async resolve => {
  const results = await _getActivityParticipates([
    'activity_id = ' + ap.activityId,
    'user_id = ' + ap.userId
  ])
  resolve(results.length === 1)
})


module.exports = {
  getArticleList,
  getArticleById,
  getActivityList,
  getActivityById,
  joinActivity,
  quitActivity,
  getAPListByActivityId,
  isAlreadyParticipate,
  getAPByUserAndActivityId,
  addActivityParticipate
}