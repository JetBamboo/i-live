const query = require('../mysql')

// 记录
const log = (log) => new Promise((resolve) => {
  query(`INSERT INTO log
    ( file, content, date ) VALUES
    ( '${log.file}', '${log.content}', '${log.date}')
  `).then((results) => {
    console.log(results)
    resolve(log)
  })
})

module.exports = {
  log
} 