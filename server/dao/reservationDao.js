const query = require('../mysql')
const Reservation = require('../bean/Reservation')
const daoUtils = require('../utils/daoUtils')

/**
 * 新增预约
 * @param {Reservation} reservation 预约对象
 * @returns {Promise}
 */
const addReservation = reservation => new Promise((resolve) => {
  const values = daoUtils.mapValues(reservation, [
    'sourceId', 'typeId', 'userId', 'name', 'phone', 'date',
    'dateStart', 'dateLive', 'budget', 'content', 'status'
  ])
  query('INSERT INTO reservation ' + values)
    .then(results => {
      reservation.id = results.insertId
      resolve(reservation)
    })
})

/**
 * 获取预约
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getReservations = where => new Promise((resolve, reject) => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM reservation' + whereStr).then(results => {
    const reservations = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new Reservation(data)
    })
    resolve(reservations)
  })
})

/**
 * 根据用户ID获取预约
 * @protected
 * @param {String|Number} id - 用户ID
 * @returns {Promise}
 */
const getReservationListByUserId = id => _getReservations('user_id = ' + id)

/**
 * 根据预约ID获取预约
 * @param {String|Number} id 预约ID
 * @returns {Promise}
 */
const getReservationById = id => new Promise(async (resolve, reject) => {
  const items = await _getReservations('id = ' + id)
  items.length === 0
    ? reject(new Error('找不到预约'))
    : resolve(items[0])
})

/**
 * 修改reservation信息
 * @param {Reservation} reservation 预约实体 
 * @returns {Promise<void>}
 */
const editReservation = reservation => new Promise(async (resolve, reject) => {
  const setters = daoUtils.mapSetters(reservation, [
    'name', 'phone', 'date', 'dateStart', 'dateLive', 'budget', 'content', 'status'
  ])
  const results = await query('UPDATE reservation SET ' + setters + ' WHERE id = ' + reservation.id)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到预约信息'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 获取所有的预约列表
 * @returns {Promise<Array<Reservation>>}
 */
const getListAll = () => new Promise(async (resolve, reject) => {
  _getReservations().then(resolve).catch(reject)
})

/**
 * 改变预约的状态
 * @param {Number|String} id 预约ID
 * @param {Number|String} status 新状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE reservation SET status = ${status} WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到预约信息'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})


module.exports = {
  _getReservations,
  getReservationListByUserId,
  addReservation,
  getReservationById,
  editReservation,
  getListAll,
  changeStatus
}
