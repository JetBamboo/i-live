const query = require('../mysql')
const daoUtils = require('../utils/daoUtils')
const Contract = require('../bean/Contract')

/**
 * 添加新合同
 * @param {Contract} contract 
 * @returns {Promise<Contract>}
 */
const add = contract => new Promise(async resolve => {
  const values = daoUtils.mapValues(contract, [
    'sourceId', 'roomId', 'userId', 'roomTypeId', 'name', 'phone',
    'urgentName', 'urgentPhone', 'location', 'email', 'card', 'date',
    'cardFront', 'cardBack', 'dateStart', 'dateEnd', 'status'
  ])
  const results = await query('INSERT INTO contract ' + values)
  contract.id = results.insertId
  resolve(contract)
})

const getListByUserId = id => new Promise(async resolve => {
  const results = await query('SELECT * FROM contract WHERE user_id = ' + id)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Contract(data)
  })
  resolve(list)
})

const getById = id => new Promise(async (resolve, reject) => {
  const results = await query('SELECT * FROM contract WHERE id = ' + id)
  if (results.length === 0) {
    return reject(new Error('没有找到合同'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Contract(data))
})

// 添加合同记录
const addLog = log => new Promise(async resolve => {
  const values = daoUtils.mapValues(log, ['contractId', 'type', 'date'])
  const results = await query('INSERT INTO contract_log ' + values)
  log.id = results.insertId
  resolve(log)
})

// 通过账单获取合同
const getByBill = bill => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM contract WHERE date = ${bill.date}`)
  if (results.length === 0) {
    return reject(new Error('没有找到合同'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Contract(data))
})

// 改变状态
const changeStatus = (id, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE contract SET status = ${status} WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到合同'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

module.exports = {
  add,
  getListByUserId,
  getById,
  addLog,
  getByBill,
  changeStatus
}