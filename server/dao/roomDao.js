const query = require('../mysql')
const RoomType = require('../bean/RoomType')
const Room = require('../bean/Room')
const daoUtils = require('../utils/daoUtils')

/**
 * 获取户型
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise<Array<RoomType>>}
 */
const _getRoomTypes = where => new Promise((resolve, reject) => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM room_type' + whereStr).then(results => {
    const roomTypes = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new RoomType(data)
    })
    resolve(roomTypes)
  })
})

/**
 * 根据房源ID获取户型列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
const getRoomTypeListBySourceId = id => _getRoomTypes('source_id = ' + id)

/**
 * 根据房源ID获取户型展示列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
const getRoomTypeDisplayListBySourceId = id => new Promise((resolve, reject) => {
  _getRoomTypes('source_id = ' + id)
    .then(roomTypes => resolve(roomTypes.slice(0, 3)))
    .catch(reject)
})

/**
 * 根据户型ID获取户型
 * @param {String|Number} id 户型ID
 * @returns {Promise<RoomType>}
 */
const getRoomTypeById = id => new Promise((resolve, reject) => {
  _getRoomTypes('id = ' + id)
    .then(roomTypes => resolve(roomTypes[0]))
    .catch(reject)
})

/**
 * 获取房间
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise<Array<Room>>}
 */
const _getRooms = where => new Promise((resolve, reject) => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM room' + whereStr).then(results => {
    const rooms = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new Room(data)
    })
    resolve(rooms)
  })
})

/**
 * 根据房源ID获取房间列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<Room>>}
 */
const getRoomListBySourceId = id => _getRooms('source_id = ' + id)

/**
 * 根据用户ID获取房间列表
 * @param {String|Number} id 用户ID
 * @returns {Promise<Array<Room>>}
 */
const getRoomListByUserId = id => _getRooms('owner_id = ' + id)

const getRoomById = id => new Promise(async (resolve, reject) => {
  const rooms = await _getRooms('id = ' + id)
  if (rooms.length === 0) {
    reject(new Error('找不到房间'))
  } else {
    resolve(rooms[0])
  }
})

/**
 * 通过房源ID和code获取房间列表
 * @param {Number|String} id 
 * @param {Promise<Array<Room>>} code 
 */
const getListSafe = (id, code) => new Promise(async (resolve, reject) => {
  _getRooms([`source_id = ${id}`, `code like '%${code}%'`])
    .then(resolve)
    .catch(reject)
})

/**
 * 改变房间的状态
 * @param {String|Number} id 房间ID
 * @param {Number} status 新状态
 * @returns {Promise<void>}
 */
const changeStatus = (id, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE room SET status = ${status} WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到房间'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 改变房间的所有人
 * @param {String|Number} id 房间ID 
 * @param {String|Number} ownerId 所有人ID
 * @returns {Promise<void>}
 */
const changeOwner = (id, ownerId) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE room SET owner_id = ${ownerId} WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到房间'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

module.exports = {
  _getRoomTypes,
  getRoomTypeById,
  getRoomTypeListBySourceId,
  getRoomTypeDisplayListBySourceId,
  _getRooms,
  getRoomListBySourceId,
  getRoomListByUserId,
  getRoomById,
  getListSafe,
  changeStatus,
  changeOwner
}
