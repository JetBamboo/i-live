const query = require('../mysql')
const User = require('../bean/User')
const daoUtils = require('../utils/daoUtils')
const UserAuth = require('../bean/UserAuth')

/**
 * 获取用户
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getUsers = where => new Promise((resolve, reject) => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM user' + whereStr).then(results => {
    const users = results.map(result => {
      const data = daoUtils.parseResult(result)
      return new User(data)
    })
    resolve(users)
  })
})

/**
 * 获取用户列表
 * @returns {Promise}
 */
const getUserList = _ => new Promise((resolve, reject) => {
  _getUsers('status != 1')
    .then(resolve)
    .catch(reject)
})

/**
 * 根据ID获取用户
 * @param {String|Number} id - 用户ID
 * @returns {Promise}
 */
const getUserById = id => new Promise(async (resolve, reject) => {
  try {
    const users = await _getUsers(['id = ' + id, 'status != 1'])
    if (users.length === 0)
      throw new Error('找不到用户')
    resolve(users[0])
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据username获取用户
 * @param {String|Number} username - 用户username
 * @returns {Promise}
 */
const getUserByUsername = username => new Promise(async (resolve, reject) => {
  try {
    const users = await _getUsers([`username = '${username}'`, 'status != 1'])
    if (users.length === 0) {
      throw new Error('找不到用户')
    }
    resolve(users[0])
  } catch (err) {
    reject(err)
  }
})

// 新增用户
const addUser = user => new Promise((resolve, reject) => {
  const values = daoUtils.mapValues(user, [
    'avatar', 'username', 'name', 'phone', 'desc', 'email',
    'status', 'dateCreate', 'thirdAccount', 'thirdName'
  ])
  query('INSERT INTO user ' + values)
    .then(results => {
      user.id = results.insertId
      resolve(user)
    })
})

const editUser = user => new Promise(async (resolve, reject) => {
  const setters = daoUtils.mapSetters(user, [
    'avatar', 'name', 'phone', 'desc', 'email', 'thirdAccount', 'thirdName'
  ])
  const results = await query('UPDATE user SET ' + setters + ' WHERE id = ' + user.id)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到用户'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 根据用户ID修改头像
 * @param {String|Number} id 用户ID
 * @param {String} avatar 用户头像
 * @returns {Promise}
 */
const changeUserAvatar = (id, avatar) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE user SET avatar = '${avatar}' WHERE id = ${id}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到用户'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 通过用户ID获取认证信息
 * @param {String|Number} id 用户ID
 * @returns {Promise<UserAuth>}
 */
const getAuthById = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM user_auth WHERE id = ${id}`)
  if (results.length === 0) {
    return reject(new Error('找不到认证信息'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new UserAuth(data))
})

/**
 * 添加新的认证信息
 * @param {UserAuth} auth 
 */
const addAuth = auth => new Promise(async resolve => {
  const values = daoUtils.mapValues(auth, [
    'id', 'compId', 'compName', 'name', 'status',
    'date', 'card', 'cardType', 'cardFront', 'cardBack'
  ])
  const results = await query('INSERT INTO user_auth ' + values)
  auth.id = results.insertId
  resolve(auth)
})

/**
 * 修改认证
 * @param {UserAuth} auth
 * @returns {Promise<UserAuth>}
 */
const editAuth = auth => new Promise(async (resolve, reject) => {
  const setters = daoUtils.mapSetters(auth, [
    'id', 'compId', 'compName', 'name', 'status',
    'date', 'card', 'cardType', 'cardFront', 'cardBack'
  ])
  const results = await query('UPDATE user_auth SET ' + setters + ' WHERE id = ' + auth.id)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到认证信息'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve(auth)
  }
})

module.exports = {
  _getUsers,
  getUserList,
  getUserById,
  getUserByUsername,
  addUser,
  editUser,
  changeUserAvatar,
  getAuthById,
  addAuth,
  editAuth
}