const query = require('../mysql')
const Source = require('../bean/Source')
const daoUtils = require('../utils/daoUtils')

/**
 * 获取房源
 * @protected
 * @param {String|Array} where - 条件
 * @returns {Promise}
 */
const _getSources = where => new Promise((resolve, reject) => {
  const whereStr = daoUtils.parseWhere(where)
  query('SELECT * FROM source' + whereStr).then(results => {
    const sources = results.map(result => {
      const data = daoUtils.parseResult(result)
      const source = new Source(data)
      // 将字符串转换成坐标数组
      source.coordinate = source.coordinate.split(',').map(c => +c)
      return source
    })
    resolve(sources)
  })
})

/**
 * 获取全部的房源列表
 * @returns {Promise}
 */
const getSourceList = _ => _getSources('status === 0')

/**
 * 根据ID获取房源
 * @param {String|Number} id 房源ID
 * @returns {Promise}
 */
const getSourceById = id => new Promise((resolve, reject) => {
  _getSources('id = ' + id)
    .then(sources => resolve(sources[0]))
    .catch(reject)
})

/**
 * 根据cityname获取房源列表
 * @param {String} city 所在城市
 * @returns {Promise}
 */
const getSourceListByCity = city => new Promise((resolve, reject) => {
  _getSources(`city = '${city}'`)
    .then(resolve)
    .catch(reject)
})

/**
 * 根据所在城市进行房源推荐
 * @param {String} city 所在城市
 * @returns {Promise}
 */
const getRecommendSourceList = city => new Promise((resolve, reject) => {
  _getSources(`city = '${city}'`)
    .then(sources => resolve(sources.slice(0, 3)))
    .catch(reject)
})

/**
 * 根据房源名称获取房源
 * @param {String} name 房源名称
 * @returns {Promise<Array<Source>>}
 */
const getListByName = name => new Promise(async (resolve, reject) => {
  _getSources(`name like '%${name}%'`)
    .then(resolve)
    .catch(reject)
})

module.exports = {
  getSourceList,
  getSourceById,
  getSourceListByCity,
  getRecommendSourceList,
  getListByName
}