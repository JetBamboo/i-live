const query = require('../mysql')
const daoUtils = require('../utils/daoUtils')
const Bill = require('../bean/Bill')
const BillWater = require('../bean/BillWater')
const BillElec = require('../bean/BillElec')
const BillRent = require('../bean/BillDeposit')
const BillDeposit = require('../bean/BillDeposit')

/**
 * 通过用户的ID获取账单列表
 * @param {Number|String} id 用户ID
 * @returns {Promise<Array<Bill>>}
 */
const getListByUserId = id => new Promise(async resolve => {
  const results = await query(`SELECT * FROM bill WHERE user_id = ${id}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new Bill(data)
  })
  resolve(list)
})

/**
 * 通过用户的ID获取账单
 * @param {Number|String} id 用户ID
 * @returns {Promise<Bill>}
 */
const getByUserId = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM bill WHERE user_id = ${id}`)
  if (results.length === 0) {
    return reject(new Error('找不到账单'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Bill(data))
})

/**
 * 通过账单的ID获取账单
 * @param {Number|String} id 账单ID
 * @returns {Promise<Bill>}
 */
const getById = id => new Promise(async (resolve, reject) => {
  const results = await query(`SELECT * FROM bill WHERE id = ${id}`)
  if (results.length === 0) {
    return reject(new Error('找不到账单'))
  }
  const data = daoUtils.parseResult(results[0])
  resolve(new Bill(data))
})

/**
 * 添加新的账单
 * @param {Bill} bill 账单实体
 * @returns {Promise<Bill>}
 */
const add = bill => new Promise(async resolve => {
  const values = daoUtils.mapValues(bill, [
    'userId', 'roomId', 'sourceId', 'roomTypeId', 'date', 'status', 'type'
  ])
  const results = await query('INSERT INTO bill ' + values)
  bill.id = results.insertId
  resolve(bill)
})

/**
 * 添加新的定金账单
 * @param {BillDeposit} deposit 定金实体
 * @returns {Promise<BillDeposit>}
 */
const addDeposit = deposit => new Promise(async resolve => {
  const values = daoUtils.mapValues(deposit, ['billId', 'date', 'price', 'status'])
  const results = await query('INSERT INTO bill_deposit ' + values)
  deposit.id = results.insertId
  resolve(deposit)
})

/**
 * 添加新的房租账单
 * @param {BillRent} rent 房租实体
 * @returns {Promise<BillRent>}
 */
const addRent = rent => new Promise(async resolve => {
  const values = daoUtils.mapValues(rent, ['billId', 'date', 'price', 'status'])
  const results = await query('INSERT INTO bill_rent ' + values)
  rent.id = results.insertId
  resolve(rent)
})

/**
 * 通过账单ID获取水费列表
 * @param {String|Number} billId 账单ID
 * @returns {Promise<Array<BillWater>>}
 */
const getWaterList = billId => new Promise(async resolve => {
  const results = await query(`SELECT * FROM bill_water WHERE bill_id = ${billId}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new BillWater(data)
  })
  resolve(list)
})

/**
 * 通过账单ID获取电费列表
 * @param {String|Number} billId 账单ID
 * @returns {Promise<Array<BillElec>>}
 */
const getElecList = billId => new Promise(async resolve => {
  const results = await query(`SELECT * FROM bill_elec WHERE bill_id = ${billId}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new BillElec(data)
  })
  resolve(list)
})

/**
 * 通过账单ID获取房租列表
 * @param {String|Number} billId 账单ID
 * @returns {Promise<Array<Electricity>>}
 */
const getRentList = billId => new Promise(async resolve => {
  const results = await query(`SELECT * FROM bill_rent WHERE bill_id = ${billId}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new BillRent(data)
  })
  resolve(list)
})

/**
 * 通过账单ID获取定金列表
 * @param {String|Number} billId 账单ID
 * @returns {Promise<Array<Electricity>>}
 */
const getDepositList = billId => new Promise(async resolve => {
  const results = await query(`SELECT * FROM bill_deposit WHERE bill_id = ${billId}`)
  const list = results.map(result => {
    const data = daoUtils.parseResult(result)
    return new BillDeposit(data)
  })
  resolve(list)
})

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeWaterStatusByBill = (billId, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE bill_water SET status = ${status} WHERE bill_id = ${billId}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到账单'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeElecStatusByBill = (billId, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE bill_elec SET status = ${status} WHERE bill_id = ${billId}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到账单'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeDepositStatusByBill = (billId, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE bill_deposit SET status = ${status} WHERE bill_id = ${billId}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到账单'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeRentStatusByBill = (billId, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE bill_rent SET status = ${status} WHERE bill_id = ${billId}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到账单'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

/**
 * 根据账单ID改变状态
 * @param {String|Number} billId 账单ID
 * @param {String|Number} status 状态
 * @returns {Promise<void>}
 */
const changeStatus = (billId, status) => new Promise(async (resolve, reject) => {
  const results = await query(`UPDATE bill SET status = ${status} WHERE id = ${billId}`)
  const message = daoUtils.parseUpdateMessage(results.message)
  if (message.matched === 0) {
    reject(new Error('找不到账单'))
  } else if (message.changed === 0) {
    reject(new Error('没有更改'))
  } else {
    resolve()
  }
})

module.exports = {
  getListByUserId,
  getByUserId,
  getById,
  add,
  addDeposit,
  addRent,
  getWaterList,
  getElecList,
  getRentList,
  getDepositList,
  changeDepositStatusByBill,
  changeElecStatusByBill,
  changeRentStatusByBill,
  changeWaterStatusByBill,
  changeStatus
}
