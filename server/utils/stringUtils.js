const {
  PRODUCTION,
  SERVER_PROTOCOL,
  SERVER_ADDRESS,
  SERVER_PORT
} = require('../config')

// 去除换行
const escapeEnter = str => str.replace(/[\r\n]/g, '')

// 去除多余空格
const escapeMutiSpace = str => str.replace(/\s+/g, ' ')

// 将对象转成请求字符串
const resolveParams = obj => {
  const clearObject = Object.create({})
  // 清除undefined, null, NaN, 只包含string, number
  Object.keys(obj).forEach(key => {
    obj[key] !== undefined
      && obj[key] !== null
      && obj[key] === obj[key]
      && ['string', 'number'].includes(typeof obj[key])
      && (clearObject[key] = obj[key])
  })
  const keys = Object.keys(clearObject)
  return keys.length !== 0
    ? '?' + keys.map(key => [key, clearObject[key]].join('=')).join('&')
    : ''
}

/**
 * 替换下划线为驼峰
 * @param {String} string 下划线字符串
 */
const underlineToCamel = string => {
  return string.replace(/_([a-z])/g, (_, s) => {
    return s.toUpperCase()
  })
}

/**
 * 替换驼峰为下划线
 * @param {String} string 驼峰字符串
 */
const camelToUnderline = string => {
  return string.replace(/([A-Z])/g, (_, s) => {
    return '_' + s.toLowerCase()
  })
}

// 返回当前服务器地址
const getServerPath = () => ''
  + SERVER_PROTOCOL
  + '://'
  + SERVER_ADDRESS
  + (PRODUCTION ? '' : ':' + SERVER_PORT)

module.exports = {
  escapeEnter,
  escapeMutiSpace,
  resolveParams,
  underlineToCamel,
  camelToUnderline,
  getServerPath
}