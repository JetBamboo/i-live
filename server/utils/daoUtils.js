const {
  underlineToCamel,
  camelToUnderline
} = require('./stringUtils')

/**
 * 解析UpdateMessage
 * @param {String} message - 结果的message
 * @returns {Object}
 */
const parseUpdateMessage = (message) => {
  const data = message.slice(6).split(' ')
  return {
    matched: data[1] - 0,
	  changed: data[4] - 0,
    warnings: data[7] - 0
  }
}

/**
 * 解析where以获得sql字符串
 * @param {String|Array} where - 条件
 * @returns {String} 
 */
const parseWhere = where => {
  let condition = ' WHERE '
  if (typeof where === 'string') {
    condition += where
  } else if (Array.isArray(where)) {
    condition += where.join(' AND ')
  } else {
    condition = ''
  }
  return condition
}

/**
 * 解析result, 深层嵌套会被过滤
 * @param {Object} result
 * @param {String} type=camel - camel=驼峰形式 underline=下标形式
 * @returns {Object}
 */
const parseResult = (result, type = 'camel') => {
  const clearObject = Object.create(null)
  const parser = type === 'camel' ? underlineToCamel : camelToUnderline
  Object.keys(result).forEach(key => {
    if (['string', 'number'].includes(typeof result[key])) {
      clearObject[parser(key)] = result[key]
    }
  })
  return clearObject
}

/**
 * 将输入转变为可被sql识别的INSERT字符串
 * @param {Entity} entity 实体
 * @param {Array} keys 需要插入的键
 * @returns {Stirng}
 */
const mapValues = (entity, keys) => {
  const pre = [], after = []
  keys.forEach(key => {
    const type = typeof entity[key]
    // console.log(key, entity[key])
    if (!['string', 'number'].includes(type)) {
      return
    }
    const _key = camelToUnderline(key)
    pre.push(['desc'].includes(_key)
      ? '`' + _key + '`'
      : _key)
    after.push(type === 'number'
      ? entity[key]
      : '\'' + entity[key] + '\'')
  })
  return `(${pre.join(', ')}) VALUES (${after.join(', ')})`
}

/**
 * 将输入转变为可被sql识别的UPDATE字符串
 * @param {Entity} entity 实体
 * @param {Array} keys 需要插入的键
 * @returns {Stirng}
 */
const mapSetters = (entity, keys) => {
  const result = []
  keys.forEach(key => {
    const type = typeof entity[key]
    if (!['string', 'number'].includes(type)) {
      return
    }
    const _key = camelToUnderline(key)
    const pre = ['desc'].includes(_key)
      ? '`' + _key + '`'
      : _key
    const after = type === 'number'
      ? entity[key]
      : '\'' + entity[key] + '\''
    result.push(`${pre} = ${after}`)
  })
  return result.join(', ')
}

module.exports = {
  parseUpdateMessage,
  parseWhere,
  parseResult,
  mapValues,
  mapSetters
}