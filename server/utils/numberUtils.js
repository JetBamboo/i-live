const BigNumber = require('bignumber.js')

/**
 * 给数字前补N个0
 * @deprecated
 * @param {*} num 
 * @param {*} length 
 */
const prefixZero = (num, length) => {
  return ('0000000' + num).slice(-length)
}

/**
 * 获取总和
 * @param {Array} list 需要统计的数组
 * @param {String} [prop] 是否是数组下的某个变量
 * @returns {Number}
 */
const getSum = (list, prop) => {
  return +list.reduce((pre, cur) => {
    const value = prop ? cur[prop] : cur
    return pre.plus(value)
  }, new BigNumber(0))
}

/**
 * 获取平均值
 * @param {Array} list 需要统计的数组
 * @param {String} [prop] 是否是数组下的某个变量
 */
const getAve = (list, prop) => {
  const sum = new BigNumber(getSum(list, prop))
  return +sum.div(list.length)
}

module.exports = {
  prefixZero,
  getSum,
  getAve
}