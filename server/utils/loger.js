// const request = require('request')
const fecha = require('fecha')
const {
  escapeEnter,
  escapeMutiSpace
} = require('./stringUtils')

class Loger {
  constructor (fileName) {
    this.fileName = fileName
  }
  info (str) {
    const date    = fecha.format(new Date(), 'YYYY-MM-DD HH:mm:ss')
    const message = escapeMutiSpace(escapeEnter(str))
    const content = `[${date}] <${this.fileName}> ${message}`
    console.log(content)
    return content
  }
  request (req) {
    let ip = req.headers['x-forwarded-for']
      || req.ip
      || req.connection.remoteAddress
      || req.socket.remoteAddress
      || req.connection.socket.remoteAddress
      || ''
    if (ip.split(',').length > 0) {
      ip = ip.split(',')[0]
    }
    const method = req.method
    const path   = req.originalUrl
    const query  = JSON.stringify(req.query)
    const body   = JSON.stringify(req.body)
    const params = JSON.stringify(req.params)
    const content = `接收请求 ${ip} ${method} ${path} query:${query} body:${body}, params:${params}`
    return this.info(content)
  }
  response (req, result) {
    // const method = req.method
    // const path   = req.originalUrl
    // const content = `响应请求 ${method} ${path} ${JSON.stringify(result)}`
    // return this.info(content)
  }
  sql (sql, result) {
    this.info(sql + ' ===> ' + JSON.stringify(result))
  }
  important (str) {
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    this.info(str)
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
  }
}

module.exports = Loger