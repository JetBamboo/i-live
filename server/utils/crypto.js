const crypto = require('crypto');

function encrypt(data, key) {
    const cipher = crypto.createCipher('md5', key);
    var crypted = cipher.update(data, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(encrypted, key) {
    const decipher = crypto.createDecipher('md5', key);
    var decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

module.exports = {
  encrypt,
  decrypt
}