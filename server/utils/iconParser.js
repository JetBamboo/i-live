const s0 = require('/assets/icons/电梯.png')
const s1 = require('/assets/icons/地铁.png')
const s2 = require('/assets/icons/便利店.png')
const s3 = require('/assets/icons/洗衣机.png')
const s4 = require('/assets/icons/停车场.png')
const s5 = require('/assets/icons/餐厅.png')
const s6 = require('/assets/icons/安全监控.png')
const s7 = require('/assets/icons/快递代收.png')
const s8 = require('/assets/icons/公共厨房.png')

const r00 = require('/assets/icons/wifi.png')
const r01 = require('/assets/icons/双人床.png')
const r02 = require('/assets/icons/灭火器.png')
const r03 = require('/assets/icons/生活用品.png')
const r04 = require('/assets/icons/电视.png')
const r05 = require('/assets/icons/冰箱.png')
const r06 = require('/assets/icons/衣架.png')
const r07 = require('/assets/icons/衣柜.png')
const r08 = require('/assets/icons/沙发.png')
const r09 = require('/assets/icons/灶台.png')
const r10 = require('/assets/icons/饮水机.png')
const r11 = require('/assets/icons/单人床.png')
const r12 = require('/assets/icons/抽油烟机.png')
const r13 = require('/assets/icons/空气净化机.png')
const r14 = require('/assets/icons/空调.png')
const r15 = require('/assets/icons/洗碗机.png')
const r16 = require('/assets/icons/洗漱台.png')
const r17 = require('/assets/icons/独立卫生间.png')
const r18 = require('/assets/icons/烤炉.png')
const r19 = require('/assets/icons/热水浴缸.png')
const r20 = require('/assets/icons/茶几.png')
const r21 = require('/assets/icons/密码锁.png')
const r22 = require('/assets/icons/微波炉.png')
const r23 = require('/assets/icons/暖气.png')

// 房源icons
module.exports.sourceIcons = [s0, s1, s2, s3, s4, s5, s6, s7, s8]

// 房间icons
module.exports.roomIcons = [
  r00, r01, r02, r03, r04, r05, r06, r07, r08, r09, r10, r11,
  r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23
]
