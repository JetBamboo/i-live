import * as user from './collection/user'
import * as upload from './collection/upload'
import * as amap from './collection/amap'
import * as source from './collection/source'
import * as room from './collection/room'
import * as reservation from './collection/reserve'
import * as community from './collection/community'
import * as repair from './collection/repair'
import * as preordain from './collection/preordain'
import * as bill from './collection/bill'
import * as contract from './collection/contract'

export default {
  user,
  upload,
  amap,
  source,
  room,
  reservation,
  community,
  repair,
  preordain,
  bill,
  contract
}
