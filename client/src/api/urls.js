import {
  SERVER_ADDRESS,
  SERVER_API_PREFIX,
  PRODUCTION
} from '@/config'

const urls = {
  user: {
    login: '/user/login',
    rootLogin: '/user/login/root',
    logout: '/user/logout',
    userList: '/user/list',
    verifyCode: '/user/verify_code',
    user: '/user',
    pin: '/user/pin',
    avatar: '/user/avatar',
    auth: '/user/auth'
  },
  source: {
    recommend: '/source/recommend',
    source: '/source',
    list: '/source/list',
    name: '/source/list/name'
  },
  room: {
    typeDisplayList: '/room/type_display_list',
    typeList: '/room/type_list',
    typeRecommend: '/room/type/recommend',
    type: '/room/type',
    userRoomList: '/room/list/user',
    safeList: '/room/list/safe',
    userUsing: '/room/list/user_using'
  },
  reserve: {
    add: '/reservation/add',
    list: '/reservation/list',
    detail: '/reservation'
  },
  community: {
    articleList: '/community/article/list',
    article: '/community/article',
    activity: '/community/activity',
    activityList: '/community/activity/list',
    joinActivity: '/community/activity/join',
    quitActivity: '/community/activity/quit'
  },
  repair: {
    detail: '/repair',
    list: '/repair/list'
  },
  preordain: {
    list: '/preordain/list',
    detail: '/preordain'
  },
  bill: {
    list: '/bill/list',
    detail: '/bill',
    pay: '/bill/pay'
  },
  contract: {
    detail: '/contract',
    list: '/contract/list'
  },
  upload: '/upload',
  amap: {
    addressTips: '/proxy/address_tips'
  }
}

const address = PRODUCTION ? '' : SERVER_ADDRESS

const prefix = (obj) => {
  Object.keys(obj).forEach(key => {
    typeof obj[key] === 'object'
      ? prefix(obj[key])
      : (obj[key] = address + '/' + SERVER_API_PREFIX + obj[key])
  })
}

prefix(urls)

export default urls
