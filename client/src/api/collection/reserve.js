import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 根据表单添加看房预约信息
 * @param {Object} form 表单字段
 * @returns {Promise}
 */
export const addReservation = form => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.post(urls.reserve.add, form)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '预约失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 获取预约列表
 * @returns {Promise}
 */
export const getReservationList = _ => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.reserve.list)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '预约列表获取失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 通过预约ID获取预约
 * @param {String|Number} id 预约ID
 * @returns {Promise}
 */
export const getReservationById = id => new Promise(async (resolve, reject) => {
  const params = { params: { id } }
  try {
    const { data } = await axios.get(urls.reserve.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '预约列表获取失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 通过预约ID取消预约
 * @param {String|Number} id 预约ID
 * @returns {Promise}
 */
export const cancelReserve = id => new Promise(async (resolve, reject) => {
  const params = { params: { id } }
  try {
    const { data } = await axios.delete(urls.reserve.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '取消预约失败')
    }
  } catch (err) {
    reject(err)
  }
})
