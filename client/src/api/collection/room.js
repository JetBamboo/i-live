import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 根据房源ID获取展示用的户型列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
export const getRoomTypeDisplayListBySourceId = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.room.typeDisplayList, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取户型展示列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据房源ID获取户型列表
 * @param {String|Number} id 房源ID
 * @returns {Promise<Array<RoomType>>}
 */
export const getRoomTypeListBySourceId = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.room.typeList, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取户型列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据户型ID获取户型
 * @param {String|Number} id 户型ID
 * @returns {Promise<RoomType>}
 */
export const getRoomTypeById = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.room.type, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取户型失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据用户ID获取房间列表
 * @returns {Promise<Array<Room>>}
 */
export const getUserRoomList = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.room.userRoomList)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房间列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据房源ID和CODE获取房间
 * @param {Number|String} id 房源ID
 * @param {String} code 房间码
 * @returns {Promise<Array<Room>>}
 */
export const getListSafe = (id, code) => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id, code } }
    const { data } = await axios.get(urls.room.safeList, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房间列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据城市获取房间推荐
 * @param {String} city 城市
 * @returns {Promise<Array<RoomType>>}
 */
export const getRecommendList = city => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { city } }
    const { data } = await axios.get(urls.room.typeRecommend, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房间列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 获取用户正在租赁的房间
 * @returns {Promise<Array<Room>>}
 */
export const getListUserUsing = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.room.userUsing)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房间列表失败')
    }
  } catch (err) {
    reject(err)
  }
})
