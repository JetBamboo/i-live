import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 提交新合同申请
 * @returns {Promise<Contract>}
 */
export const add = contract => new Promise(async (resolve, reject) => {
  console.log(contract)
  try {
    const { data } = await axios.post(urls.contract.detail, contract)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '添加合同失败')
    }
  } catch (err) {
    reject(err)
  }
})

export const getList = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.contract.list)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取合同列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

export const getById = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.contract.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取合同失败')
    }
  } catch (err) {
    reject(err)
  }
})
