import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 获取所有文章列表
 * @returns {Promise}
 */
export const getArticleList = _ => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.community.articleList)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取文章列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据文章ID获取文章
 * @param {Number|String} id 文章ID
 * @returns {Promise}
 */
export const getArticleById = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.community.article, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取文章失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 获取所有活动列表
 * @returns {Promise}
 */
export const getActivityList = _ => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.community.activityList)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取活动动列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据活动ID获取活动
 * @param {Number|String} id 活动ID
 * @returns {Promise}
 */
export const getActivityById = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.community.activity, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取活动失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 通过活动ID来参加活动
 * @param {String|Number} id 活动ID
 * @returns {Promise}
 */
export const joinActivity = id => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.post(urls.community.joinActivity, { id })
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '参加活动失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 通过活动ID来取消参加活动
 * @param {String|Number} id 活动ID
 * @returns {Promise}
 */
export const quitActivity = id => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.put(urls.community.quitActivity, { id })
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '取消活动参与失败')
    }
  } catch (err) {
    reject(err)
  }
})
