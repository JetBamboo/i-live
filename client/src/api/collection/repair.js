import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 获取维修列表
 * @returns {Promise<Array<Repair>>}
 */
export const getRepairList = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.repair.list)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取维修列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据ID获取维修详情
 * @param {Number|String} id 维修ID
 * @returns {Promise<Repair>}
 */
export const getDetail = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.repair.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取维修详情失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据表单添加维修信息
 * @param {Object} form 表单
 * @returns {Promise<Repair>}
 */
export const add = form => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.post(urls.repair.detail, form)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '添加维修信息失败')
    }
  } catch (err) {
    reject(err)
  }
})
