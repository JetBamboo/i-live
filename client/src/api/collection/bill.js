import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 获取所有账单列表
 * @returns {Promise<Array<Bill>>}
 */
export const getList = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.bill.list)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取账单列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 通过账单ID获取账单详情
 * @param {String|Number} id 账单ID
 * @returns {Promise<Bill>}
 */
export const getDetail = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.bill.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取账单失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 支付账单
 * @param {String|Number} id 账单ID
 * @returns {Promise<Bill>}
 */
export const pay = id => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.put(urls.bill.pay, { id })
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '支付账单失败')
    }
  } catch (err) {
    reject(err)
  }
})
