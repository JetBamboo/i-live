import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS, FILE_TYPE_IMAGE } from '@/config'

export const upload = (file, type) => new Promise((resolve, reject) => {
  const formData = new FormData()
  formData.append(type, file, file.name)
  axios({
    url: urls.upload + '/' + type,
    method: 'post',
    anync: true,
    contentType: false,
    processData: false,
    data: formData
  }).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Upload failed!'))
  }).catch(reject)
})

export const uploadImage = (image) => upload(image, FILE_TYPE_IMAGE)
