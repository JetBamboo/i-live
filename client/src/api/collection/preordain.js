import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 用户获取预订列表
 * @returns {Promise<Array<Preordain>>}
 */
export const getPreordainList = () => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.get(urls.preordain.list)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取预订列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 用户添加预定信息
 * @param {Preordain} form 数据
 * @returns {Promise<void>}
 */
export const add = form => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.post(urls.preordain.detail, form)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取预订列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 获取预定详情
 * @param {Number|String} id 预定ID
 * @returns {Promise<Preordain>}
 */
export const getDetail = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.preordain.detail, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取预订失败')
    }
  } catch (err) {
    reject(err)
  }
})
