import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

export const getUserDetail = id => {
  return new Promise((resolve, reject) => {
    const params = { id }
    axios.get(urls.user.detail, { params }).then(({ data }) => {
      data.code === CODE_SUCCESS
        ? resolve(data.value)
        : reject(new Error(data.desc || 'Get user detail failed!'))
    }).catch(reject)
  })
}

export const getUserList = _ => new Promise((resolve, reject) => {
  axios.get(urls.user.userList).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Get user list failed!'))
  }).catch(reject)
})

export const getVerifyCode = phone => new Promise((resolve, reject) => {
  const params = { phone }
  axios.get(urls.user.verifyCode, { params }).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Get verify code failed!'))
  }).catch(reject)
})

export const login = (phone, code) => new Promise((resolve, reject) => {
  axios.post(urls.user.login, { phone, code }).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Login failed!'))
  }).catch(reject)
})

// 用管理员账户登录
export const loginWithRoot = () => new Promise((resolve, reject) => {
  axios.post(urls.user.rootLogin).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || '登录失败'))
  }).catch(reject)
})

export const logout = _ => new Promise((resolve, reject) => {
  axios.get(urls.user.logout).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Logout failed!'))
  }).catch(reject)
})

export const getUserWithPin = _ => new Promise((resolve, reject) => {
  axios.get(urls.user.pin).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'Get user failed!'))
  }).catch(reject)
})

// 修改用户头像
export const changeUserAvatar = url => new Promise((resolve, reject) => {
  axios.put(urls.user.avatar, { url }).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || '改变用户的头像失败!'))
  }).catch(reject)
})

export const editUser = user => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.put(urls.user.user, { user })
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '修改用户信息失败')
    }
  } catch (err) {
    reject(err)
  }
})

// 修改认证信息
export const editAuth = form => new Promise(async (resolve, reject) => {
  try {
    const { data } = await axios.put(urls.user.auth, { form })
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '修改认证信息失败')
    }
  } catch (err) {
    reject(err)
  }
})
