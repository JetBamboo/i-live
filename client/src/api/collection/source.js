import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

/**
 * 根据所在城市进行房源推荐
 * @param {String} city 所在城市
 * @returns {Promise}
 */
export const getRecommendSourceList = city => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { city } }
    const { data } = await axios.get(urls.source.recommend, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房源推荐列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据所在城市获取房源列表
 * @param {String} city 所在城市
 * @returns {Promise}
 */
export const getSourceList = city => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { city } }
    const { data } = await axios.get(urls.source.list, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房源列表失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据ID获取房源
 * @param {String|Number} id 房源ID
 * @returns {Promise}
 */
export const getSourceById = id => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { id } }
    const { data } = await axios.get(urls.source.source, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房源失败')
    }
  } catch (err) {
    reject(err)
  }
})

/**
 * 根据房源名称获取房源
 * @param {String} name 房源名称
 * @returns {Promise}
 */
export const getListByName = name => new Promise(async (resolve, reject) => {
  try {
    const params = { params: { name } }
    if (name.includes('\'')) {
      return resolve([])
    }
    const { data } = await axios.get(urls.source.name, params)
    if (data.code === CODE_SUCCESS) {
      resolve(data.value)
    } else {
      throw new Error(data.desc || '获取房源失败')
    }
  } catch (err) {
    reject(err)
  }
})
