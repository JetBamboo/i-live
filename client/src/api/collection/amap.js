import axios from '@/utils/axios'
import urls from '../urls'
import { CODE_SUCCESS } from '@/config'

export const getAddressTips = (keywords, city) => new Promise((resolve, reject) => {
  const params = { keywords, city }
  axios.get(urls.amap.addressTips, { params }).then(({ data }) => {
    data.code === CODE_SUCCESS
      ? resolve(data.value)
      : reject(new Error(data.desc || 'get input tips failed!'))
  }).catch(reject)
})
