import { ENV, SERVER_ADDRESS } from '@/config'
import BigNumber from 'bignumber.js'

/** 获得一个纯净的空对象
 * @returns {Object}
 */
export const getClearObject = _ => Object.create(null)

/**
 * 判断是否是空对象
 * @param {Object} o
 * @returns {Boolean}
 */
export const isEmptyObject = o => Object.keys(o).length === 0

/**
 * 给数字前补n个0
 * @param {Number|String} num 需要补零的数字
 * @param {Number} length 补0的数量
 * @returns {String} 补零后的字符串
 */
export const prefixZero = (num, length) => {
  return ('0000000' + num).slice(-length)
}

/**
 * 产生[min, max)的整型随机数
 * @param {Number} min 最小值
 * @param {Number} max 最大值(取不到)
 * @returns {Number} 整型随机数
 */
export const random = (min, max) => {
  return min + Math.random() * (max - min) | 0
}

/**
 * 判断是不是偶数
 * @param {Number} num
 * @returns {Number} 是不是偶数
 */
export const isEven = (num) => num % 2 === 0

/**
 * 判断是不是奇数
 * @param {Number} num
 * @returns {Number} 是不是奇数
 */
export const isOdd = (num) => !isEven(num)

/**
 * 解析cookie字符串
 * @param {String} cookie
 * @returns {Object} 解析后的cookie对象
 */
export const parseCookie = cookie => {
  const result = Object.create(null)
  cookie.split('; ').forEach(item => {
    const kv = item.split('=')
    result[kv[0]] = kv[1]
  })
  return result
}

/**
 * 将Object转换成可嵌入dom的style字符串
 * @param {Object} o style对象
 * @returns {String} style字符串
 */
export const parseStyle = o => {
  return Object.keys(o).map(key => `${key}:${o[key]}`).join(';')
}

/**
 * 将数组转换成可嵌入dom的class字符串
 * @param {Object|Array} o class数组或类数组
 * @returns {String} class字符串
 */
export const parseClass = o => {
  return Array.from(o).join(' ')
}

/**
 * 将时间戳转换成xxxx年x月x日 xx:xx的形式
 * @deprecated
 * @param {Number|String} timestamp 时间戳
 * @returns {String} 时间字符串
 */
export const parseTimestamp = timestamp => {
  const date = new Date(timestamp)
  const year = date.getFullYear()
  const month = prefixZero(date.getMonth() + 1, 2)
  const day = prefixZero(date.getDate(), 2)
  const hours = prefixZero(date.getHours(), 2)
  const minutes = prefixZero(date.getMinutes(), 2)
  return `${year}-${month}-${day} ${hours}:${minutes}`
}

/** 判断是否是电话号码
 * @param {String|Number} a 电话
 * @returns {Boolean} 是否是电话号码
 */
export const isPhone = a => /^1[34578]\d{9}$/.test(a)

/** 判断是否是邮箱
 * @param {String|Number} a 邮箱
 * @returns {Boolean} 是否是邮箱
 */
export const isEmail = a => /^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/.test(a)

/** 判断是否是身份证
 * @param {String|Number} a 身份证
 * @returns {Boolean} 是否是身份证
 */
export const isCard = a => /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|31)|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/.test(a)

/** 判断字符串是否为空
 * @param {Stirng} s 字符串
 * @returns {Boolean} 字符串是否为空
 */
export const isEmpty = s => !s

/** 判断字符串是否不为空
 * @param {String} s 字符串
 * @returns {Boolean} 字符串是不否为空
 */
export const isNotEmpty = s => !!s

/** 判断长度是否满足
 * @param {String|Array} a // 目标
 * @param {Number} l // 长度
 * @returns {Boolean} 长度是否满足
 */
export const hasLenthOf = (a, l) => a.length === l

/** 判断是否是6位验证码
 * @param {String|Number} a // 目标
 * @returns {Boolean} 是否是6位验证码
 */
export const isCode = a => hasLenthOf(a, 6)

/** 判断是否是生产环境
 * @returns {Boolean} 是否是生产环境
 */
export const isProduction = _ => ENV === 'production'

/** 判断是否是开发环境
 * @returns {Boolean} 是否是开发环境
 */
export const isDevelopment = _ => ENV === 'development'

/**
 * 获取总和
 * @param {Array} list 需要统计的数组
 * @param {String} [prop] 是否是数组下的某个变量
 * @returns {Number}
 */
export const getSum = (list, prop) => {
  return +list.reduce((pre, cur) => {
    const value = prop ? cur[prop] : cur
    return pre.plus(value)
  }, new BigNumber(0))
}

/**
 * 获取平均值
 * @param {Array} list 需要统计的数组
 * @param {String} [prop] 是否是数组下的某个变量
 * @returns {Number}
 */
export const getAve = (list, prop) => {
  const sum = new BigNumber(getSum(list, prop))
  return +sum.div(list.length)
}

/**
 * 将所有包含图片的{base}字段的地址全替换为服务器地址
 * @param {String} str 包含图片的地址
 */
export const parseImage = (str) => {
  return str.replace(/{base}/g, SERVER_ADDRESS)
}
