import axios from 'axios'
// import qs from 'qs'

const myAxios = axios.create({
  timeout: 30000,
  withCredentials: true
})

// myAxios.defaults.transformRequest = [(data, config) => {
//   if (!config['Content-Type']) return qs.stringify(data)
//   switch (config['Content-Type'].toLowerCase()) {
//     // 返回得到一个对象(可以理解为json或是一个数组)
//     case 'application/json;charset=utf-8': {
//       return JSON.stringify(data)
//     }
//     // 当前如果是formData属性的配置时, 直接将当前的data返回
//     case 'multipart/form-data;charset=utf-8': {
//       return data
//     }
//     // 默认返回qs序列化后的内容, 防止内容没有进行处理
//     default: {
//       return qs.stringify(data)
//     }
//   }
// }]

export default myAxios
