import * as colors from 'muse-ui/lib/theme/colors'

export default {
  primary: '#ea7777',
  secondary: colors.grey600,
  success: colors.blue500,
  warning: colors.amber500,
  danger: colors.red500,
  background: colors.grey200,
  line: colors.grey300,
  white: colors.grey50,
  black: colors.grey900,
  blackl: colors.grey600
}
