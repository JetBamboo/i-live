import fecha from 'fecha'

export const format = (timestamp, mask) => {
  if (timestamp === undefined || timestamp === '') {
    return ''
  }
  return fecha.format(new Date(timestamp), mask)
}

export const parse = (time, mask) => {
  return fecha.parse(time, mask)
}
