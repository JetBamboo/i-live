export const nearbyMapper = [
  /* 0 */ { name: '电梯', icon: require('@/assets/images/icons/电梯.png') },
  /* 1 */ { name: '地铁', icon: require('@/assets/images/icons/地铁.png') },
  /* 2 */ { name: '洗衣机', icon: require('@/assets/images/icons/洗衣机.png') },
  /* 3 */ { name: '停车场', icon: require('@/assets/images/icons/停车场.png') },
  /* 4 */ { name: '餐厅', icon: require('@/assets/images/icons/餐厅.png') },
  /* 5 */ { name: '便利店', icon: require('@/assets/images/icons/便利店.png') },
  /* 6 */ { name: '安全监控', icon: require('@/assets/images/icons/安全监控.png') },
  /* 7 */ { name: '快递代收', icon: require('@/assets/images/icons/快递代收.png') },
  /* 8 */ { name: '公共厨房', icon: require('@/assets/images/icons/公共厨房.png') }
]

export const furnitureMapper = [
  /* 00 */ { name: 'wifi', icon: require('@/assets/images/icons/wifi.png') },
  /* 01 */ { name: '单人床', icon: require('@/assets/images/icons/单人床.png') },
  /* 02 */ { name: '双人床', icon: require('@/assets/images/icons/双人床.png') },
  /* 03 */ { name: '灭火器', icon: require('@/assets/images/icons/灭火器.png') },
  /* 04 */ { name: '生活用品', icon: require('@/assets/images/icons/生活用品.png') },
  /* 05 */ { name: '衣架', icon: require('@/assets/images/icons/衣架.png') },
  /* 06 */ { name: '衣柜', icon: require('@/assets/images/icons/衣柜.png') },
  /* 07 */ { name: '沙发', icon: require('@/assets/images/icons/沙发.png') },
  /* 08 */ { name: '灶台', icon: require('@/assets/images/icons/灶台.png') },
  /* 09 */ { name: '饮水机', icon: require('@/assets/images/icons/饮水机.png') },
  /* 10 */ { name: '抽油烟机', icon: require('@/assets/images/icons/抽油烟机.png') },
  /* 11 */ { name: '空气净化机', icon: require('@/assets/images/icons/空气净化机.png') },
  /* 12 */ { name: '洗碗机', icon: require('@/assets/images/icons/洗碗机.png') },
  /* 13 */ { name: '洗漱台', icon: require('@/assets/images/icons/洗漱台.png') },
  /* 14 */ { name: '独立卫生间', icon: require('@/assets/images/icons/独立卫生间.png') },
  /* 15 */ { name: '烤炉', icon: require('@/assets/images/icons/烤炉.png') },
  /* 16 */ { name: '热水浴缸', icon: require('@/assets/images/icons/热水浴缸.png') },
  /* 17 */ { name: '茶几', icon: require('@/assets/images/icons/茶几.png') },
  /* 18 */ { name: '密码锁', icon: require('@/assets/images/icons/密码锁.png') },
  /* 19 */ { name: '微波炉', icon: require('@/assets/images/icons/微波炉.png') },
  /* 20 */ { name: '暖气', icon: require('@/assets/images/icons/暖气.png') },
  /* 21 */ { name: '电视', icon: require('@/assets/images/icons/电视.png') },
  /* 22 */ { name: '冰箱', icon: require('@/assets/images/icons/冰箱.png') },
  /* 23 */ { name: '空调', icon: require('@/assets/images/icons/空调.png') }
]
