// env
export const ENV = process.env.NODE_ENV
export const DEVELOPMENT = ENV === 'development'
export const PRODUCTION = ENV === 'production'

// server
export const SERVER_ADDRESS = PRODUCTION ? 'http://39.108.107.156' : 'http://127.0.0.1:8088'
export const SERVER_API_PREFIX = 'api'

// router
export const ROUTER_PASSTHROUGH = DEVELOPMENT && false

// response code
export const CODE_SUCCESS = 0
export const CODE_ERROR = 1
export const CODE_NOT_LOGIN = 2

// upload file type
export const FILE_TYPE_IMAGE = 'image'
export const FILE_TYPE_FILE = 'file'
