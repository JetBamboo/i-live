import AppFooter from './AppFooter.vue'
import AppHeader from './AppHeader.vue'
import CityPicker from './CityPicker.vue'
import EmptyListholder from './EmptyListholder.vue'
import ImageUploader from './ImageUploader.vue'
import MyImage from './MyImage.vue'
import Rowlist from './Rowlist.vue'
import Scroll from './Scroll.vue'
import Topbar from './Topbar.vue'
import Wave from './Wave.vue'
import Whitespace from './Whitespace.vue'

const components = [
  AppFooter,
  AppHeader,
  CityPicker,
  EmptyListholder,
  ImageUploader,
  MyImage,
  Rowlist,
  Scroll,
  Topbar,
  Wave,
  Whitespace
]

const loader = {
  install: Vue => {
    components.forEach(component => {
      Vue.component(component.name, component)
    })
  }
}

export default loader
