export const SET_USER = 'SET_USER'

export const SET_USER_AVATAR = 'SET_USER_AVATAR'

export const FIRST_OPEN = 'FIRST_OPEN'

export const SET_ADDRESS = 'SET_ADDRESS'

export const SHOW_NAV = 'SHOW_NAV'

export const SET_FORBID = 'SET_FORBID'

export const SET_FROM = 'SET_FROM'

export const SET_AUTH = 'SET_AUTH'
