import * as types from './mutation-types'

export default {
  [types.SET_USER] (state, user) {
    state.user = Object.assign({}, state.user, user)
  },
  [types.SET_USER_AVATAR] (state, avatar) {
    state.user.avatar = avatar
  },
  [types.FIRST_OPEN] (state) {
    state.firstOpen = false
  },
  [types.SET_ADDRESS] (state, address) {
    state.address = Object.assign({}, state.address, address)
  },
  [types.SHOW_NAV] (state, showNav = true) {
    state.navDisplay = showNav
  },
  [types.SET_FORBID] (state, forbid = true) {
    state.forbid = forbid
  },
  [types.SET_FROM] (state, from = '') {
    state.from = from
  },
  [types.SET_AUTH] (state, auth) {
    console.log('before', state.user.auth, auth)
    state.user.auth = Object.assign(state.user.auth, auth)
    console.log('after', state.user.auth)
  }
}
