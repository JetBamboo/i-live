export default {
  user: {
    id: -1,
    auth: {}
  }, // 用户数据
  firstOpen: true, // 第一次打开app [弃用]
  address: { // 用户所在地址
    province: '湖南',
    city: '长沙',
    code: 10000,
    coordinate: [0, 0],
    complete: false // 是否加载完成
  },
  navDisplay: true, // 是否显示底部tab
  forbid: true, // 受保护的网页是否能打开
  from: '' // 来之前的路由
}
