import Vue from 'vue'
import Router from 'vue-router'

import routes from './routes'
import guards from './guards'

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  routes
})

router.beforeEach(guards)

export default router
