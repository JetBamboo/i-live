import { parseCookie } from '@/utils/utils'
import store from '@/store/index'
import * as mutationTypes from '@/store/mutation-types'
import { ROUTER_PASSTHROUGH } from '@/config'

const is = function (to, attr) {
  return to.matched.some(m => m.meta[attr])
}

export default (to, from, next) => {
  // 保留上一次的地址
  store.commit(mutationTypes.SET_FROM, from.name)

  if (ROUTER_PASSTHROUGH) return next()

  // 访问的页面是否受保护 (无法通过url直接进入)
  if (is(to, 'forbid')) {
    console.log(to.fullPath, '受保护')
    if (store.state.forbid) {
      return next({ name: 'error-403' })
    }
  }

  // 访问页面是否需要登录
  if (is(to, 'needAuth')) {
    console.log(to.fullPath, '需要登录')
    return parseCookie(document.cookie).pin
      ? next()
      : next({ name: 'login-phone' })
  }

  // 通过校验, 导航到页面
  next()
}
