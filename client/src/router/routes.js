export default [
  { // root
    path: '/',
    redirect: 'home'
  },
  { // home
    path: '/home',
    name: 'home',
    component: () => import('@/views/Home.vue')
  },
  { // source
    path: '/source',
    component: () => import('@/views/source/Source.vue'),
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'list',
        name: 'source-list',
        component: () => import('@/views/source/List.vue')
      },
      {
        path: 'find',
        name: 'source-find',
        component: () => import('@/views/source/Find.vue')
      },
      {
        path: 'detail',
        name: 'source-detail',
        component: () => import('@/views/source/Detail.vue')
      },
      {
        path: 'recommend',
        name: 'source-recommend',
        component: () => import('@/views/source/Recommend.vue')
      }
    ]
  },
  { // room
    path: '/room',
    component: () => import('@/views/room/Room.vue'),
    children: [
      {
        path: '',
        redirect: 'recommend'
      },
      {
        path: 'recommend',
        name: 'room-recommend',
        component: () => import('@/views/room/Recommend.vue')
      },
      {
        path: 'list',
        name: 'room-list',
        component: () => import('@/views/room/List.vue')
      },
      {
        path: 'detail',
        name: 'room-detail',
        component: () => import('@/views/room/Detail.vue')
      }
    ]
  },
  { // community
    path: '/community',
    name: 'community',
    component: () => import('@/views/community/Community.vue')
  },
  { // article
    path: '/article',
    component: () => import('@/views/community/Article.vue'),
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'list',
        name: 'article-list',
        component: () => import('@/views/community/ArticleList.vue')
      },
      {
        path: 'detail',
        name: 'article-detail',
        component: () => import('@/views/community/ArticleDetail.vue')
      }
    ]
  },
  { // activity
    path: '/activity',
    component: () => import('@/views/community/Activity.vue'),
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'list',
        name: 'activity-list',
        component: () => import('@/views/community/ActivityList.vue')
      },
      {
        path: 'detail',
        name: 'activity-detail',
        component: () => import('@/views/community/ActivityDetail.vue')
      }
    ]
  },
  { // profile
    path: '/profile',
    component: () => import('@/views/profile/Profile.vue'),
    children: [
      {
        path: '',
        redirect: 'info'
      },
      {
        path: 'detail',
        name: 'profile-detail',
        component: () => import('@/views/profile/Detail.vue'),
        meta: { needAuth: true }
      },
      {
        path: 'info',
        name: 'profile-info',
        component: () => import('@/views/profile/Info.vue')
      },
      {
        path: 'message',
        name: 'profile-message',
        component: () => import('@/views/profile/Message.vue'),
        meta: { needAuth: true }
      },
      {
        path: 'auth',
        name: 'profile-auth',
        component: () => import('@/views/profile/Auth.vue'),
        meta: { needAuth: true }
      }
    ]
  },
  { // login
    path: '/login',
    component: () => import('@/views/login/Login.vue'),
    children: [
      {
        path: '',
        redirect: 'phone'
      },
      {
        path: 'phone',
        name: 'login-phone',
        component: () => import('@/views/login/Phone.vue')
      }
    ]
  },
  { // contract
    path: '/contract',
    component: () => import('@/views/contract/Contract.vue'),
    meta: { needAuth: true },
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'detail',
        name: 'contract-detail',
        component: () => import('@/views/contract/Detail.vue')
      },
      {
        path: 'list',
        name: 'contract-list',
        component: () => import('@/views/contract/List.vue')
      },
      {
        path: 'add',
        name: 'contract-add',
        component: () => import('@/views/contract/Add.vue')
      }
    ]
  },
  { // reserve
    path: '/reserve',
    component: () => import('@/views/reserve/Reserve.vue'),
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'add',
        name: 'reserve-add',
        component: () => import('@/views/reserve/Add.vue')
      },
      {
        path: 'list',
        name: 'reserve-list',
        component: () => import('@/views/reserve/List.vue')
      },
      {
        path: 'detail',
        name: 'reserve-detail',
        component: () => import('@/views/reserve/Detail.vue')
      }
    ]
  },
  { // preorder
    path: '/preorder',
    component: () => import('@/views/preorder/Preorder.vue'),
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'add',
        name: 'preorder-add',
        component: () => import('@/views/preorder/Add.vue')
      },
      {
        path: 'list',
        name: 'preorder-list',
        component: () => import('@/views/preorder/List.vue')
      },
      {
        path: 'detail',
        name: 'preorder-detail',
        component: () => import('@/views/preorder/Detail.vue')
      }
    ]
  },
  { // bill
    path: '/bill',
    component: () => import('@/views/bill/Bill.vue'),
    meta: { needAuth: true },
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'list',
        name: 'bill-list',
        component: () => import('@/views/bill/List.vue')
      },
      {
        path: 'detail',
        name: 'bill-detail',
        component: () => import('@/views/bill/Detail.vue')
      },
      {
        path: 'meter',
        name: 'bill-meter',
        component: () => import('@/views/bill/Meter.vue')
      },
      {
        path: 'water',
        name: 'bill-water',
        component: () => import('@/views/bill/Water.vue')
      },
      {
        path: 'electricity',
        name: 'bill-electricity',
        component: () => import('@/views/bill/Electricity.vue')
      },
      {
        path: 'pay',
        name: 'bill-pay',
        meta: { forbid: true },
        component: () => import('@/views/bill/Pay.vue')
      },
      {
        path: 'paid',
        name: 'bill-paid',
        meta: { forbid: true },
        component: () => import('@/views/bill/Paid.vue')
      }
    ]
  },
  { // repair
    path: '/repair',
    component: () => import('@/views/repair/Repair.vue'),
    meta: { needAuth: true },
    children: [
      {
        path: '',
        redirect: 'list'
      },
      {
        path: 'list',
        name: 'repair-list',
        component: () => import('@/views/repair/List.vue')
      },
      {
        path: 'detail',
        name: 'repair-detail',
        component: () => import('@/views/repair/Detail.vue')
      },
      {
        path: 'Add',
        name: 'repair-add',
        component: () => import('@/views/repair/Add.vue')
      }
    ]
  },
  { // about
    path: '/about',
    name: 'about',
    component: () => import('@/views/About.vue')
  },
  { // feedback
    path: '/feedback',
    name: 'feedback',
    component: () => import('@/views/Feedback.vue')
  },
  { // error
    path: '/error',
    component: () => import('@/views/error/Error.vue'),
    children: [
      {
        path: '',
        redirect: '404'
      },
      {
        path: '404',
        name: 'error-404',
        component: () => import('@/views/error/E404.vue')
      },
      {
        path: '403',
        name: 'error-403',
        component: () => import('@/views/error/E403.vue')
      }
    ]
  },
  { // 404
    path: '*',
    redirect: '/error/404'
  }
]
