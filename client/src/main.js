import Vue from 'vue'
import './registerServiceWorker'
// import FastClick from 'fastclick'
import MuseUI from 'muse-ui'
import theme from 'muse-ui/lib/theme'
import Message from 'muse-ui-message'
import Toast from 'muse-ui-toast'
import NProgress from 'muse-ui-progress'
import VueLazyload from 'vue-lazyload'
import Loading from 'muse-ui-loading'
import color from '@/utils/color'
import * as dateUtils from '@/utils/date'
import App from '@/App.vue'
import router from '@/router/index'
import store from '@/store/index'
import MyComponents from '@/components/index'
import api from '@/api/index'

import 'muse-ui/dist/muse-ui.css'
import 'muse-ui-progress/dist/muse-ui-progress.css'
import 'typeface-roboto'
import '@/assets/scss/mu-custom.scss'
import '@/assets/scss/custom.scss'
import 'muse-ui-loading/dist/muse-ui-loading.css'

Vue.config.productionTip = false

// FastClick config
// FastClick.attach(document.body)

// my components config
Vue.use(MyComponents)

// lazy load config
Vue.use(VueLazyload)

// Muse-ui config
theme.add('ilive', { primary: color.primary, secondary: color.secondary }, 'light')
theme.use('ilive')

Vue.use(MuseUI)
Vue.use(Message)
Vue.use(Toast)
Vue.use(NProgress)
Vue.use(Loading)

// Utils config
Vue.prototype.$color = color
Vue.prototype.$f = dateUtils.format
Vue.prototype.$p = dateUtils.parse
Vue.prototype.$api = api

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
