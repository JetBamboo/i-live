module.exports = {
  productionSourceMap: false,
  chainWebpack: config => {
    config.devServer.disableHostCheck(true) // 禁用host检查, 使其支持外网访问
  },
  pwa: {
    name: '艾住',
    themeColor: '#ffffff',
    msTileColor: '#ffffff'
  },
  publicPath: './', // 一定要有, 要不然找不到资源
  outputDir: '../server/public/client', // 转到server项目的public中去
  devServer: {
    port: 8888,
  },
}