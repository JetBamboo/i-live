module.exports = {
  root: true,
  env: {
    node: true
  },
  globals: {
    AMap: true,
    ClipboardJS: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'operator-linebreak': 'off',
    'brace-style': 'off',
    'no-extra-boolean-cast': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
