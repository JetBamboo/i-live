/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50550
Source Host           : localhost:3306
Source Database       : ilive

Target Server Type    : MYSQL
Target Server Version : 50550
File Encoding         : 65001

Date: 2019-05-20 19:32:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `source_id` int(11) NOT NULL DEFAULT '0' COMMENT '房源ID',
  `title` varchar(255) DEFAULT NULL COMMENT '活动标题',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `date_publish` bigint(20) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `date_start` bigint(20) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `date_end` bigint(20) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `content` text COMMENT '活动内容，html文本',
  PRIMARY KEY (`id`,`source_id`),
  KEY `id` (`id`),
  KEY `source_id` (`source_id`),
  CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动表';

-- ----------------------------
-- Table structure for activity_participate
-- ----------------------------
DROP TABLE IF EXISTS `activity_participate`;
CREATE TABLE `activity_participate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '活动报名表ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '参与用户id',
  `activity_id` int(11) NOT NULL DEFAULT '0' COMMENT '活动ID',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '参与时间',
  PRIMARY KEY (`id`,`user_id`,`activity_id`),
  KEY `user_id` (`user_id`),
  KEY `activity_participate_ibfk_1` (`activity_id`),
  CONSTRAINT `activity_participate_ibfk_1` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `activity_participate_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动参与表';

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `source_id` int(11) NOT NULL DEFAULT '0' COMMENT '房源ID',
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `content` text COMMENT '内容',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '文章类型，0=艾住精选，1=艾住故事',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  PRIMARY KEY (`id`,`source_id`),
  KEY `source_id` (`source_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Table structure for assessment
-- ----------------------------
DROP TABLE IF EXISTS `assessment`;
CREATE TABLE `assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评价ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `resource_id` int(11) NOT NULL COMMENT '评价源ID',
  `resource_type` tinyint(4) NOT NULL COMMENT '评价类型，0=账单评价，1=看房评价，2=报修评价',
  `rate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '星级，几星评价',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '评价时间',
  `content` text COMMENT '评价内容',
  PRIMARY KEY (`id`,`user_id`,`resource_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `assessment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评价表';

-- ----------------------------
-- Table structure for bill
-- ----------------------------
DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账单ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `water_id` int(11) NOT NULL COMMENT '水表ID',
  `electricity_id` int(11) NOT NULL COMMENT '电表ID',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '账单生成时间',
  `room_bill` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '房价',
  PRIMARY KEY (`id`,`user_id`,`room_id`,`water_id`,`electricity_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  KEY `water_id` (`water_id`),
  KEY `electricity_id` (`electricity_id`),
  KEY `id` (`id`),
  CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `bill_ibfk_3` FOREIGN KEY (`water_id`) REFERENCES `water` (`id`),
  CONSTRAINT `bill_ibfk_4` FOREIGN KEY (`electricity_id`) REFERENCES `electricity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账单表';

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `id` int(11) NOT NULL COMMENT '收藏ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `room_type_id` int(11) NOT NULL COMMENT '房间类型ID',
  PRIMARY KEY (`id`,`user_id`,`room_type_id`),
  KEY `user_id` (`user_id`),
  KEY `room_type_id` (`room_type_id`),
  CONSTRAINT `collection_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `collection_ibfk_2` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Table structure for contract
-- ----------------------------
DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '合同ID',
  `source_id` int(11) NOT NULL COMMENT '房源ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '合同签订时间',
  `date_start` bigint(20) NOT NULL DEFAULT '0' COMMENT '合同生效时间',
  `date_end` bigint(20) NOT NULL DEFAULT '0' COMMENT '合同失效时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态; 0=待交首期款,1=待审核,2=待入住,3=租赁中,4=待退押金,5=合同中止,10=合同未生效',
  `code` varchar(24) NOT NULL DEFAULT '000000000000000000000000' COMMENT '编号,生成合同时生成的24位16进制编号',
  PRIMARY KEY (`id`,`source_id`,`room_id`,`user_id`),
  KEY `source_id` (`source_id`),
  KEY `room_id` (`room_id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`),
  CONSTRAINT `contract_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `contract_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='合同表';

-- ----------------------------
-- Table structure for contract_log
-- ----------------------------
DROP TABLE IF EXISTS `contract_log`;
CREATE TABLE `contract_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=签署合同,1=交首期款,2=办理入住,3=办理退房,4=退回押金,5=合同终止,6=合同未生效',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '发生事件的时间',
  PRIMARY KEY (`id`,`contract_id`),
  KEY `contract_id` (`contract_id`),
  CONSTRAINT `contract_log_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='合同消息记录表';

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '优惠券ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '优惠价格',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态,0=未使用,1=已使用',
  PRIMARY KEY (`id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  CONSTRAINT `coupon_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券表';

-- ----------------------------
-- Table structure for coupon_log
-- ----------------------------
DROP TABLE IF EXISTS `coupon_log`;
CREATE TABLE `coupon_log` (
  `id` int(11) NOT NULL COMMENT '优惠券记录id',
  `coupon_id` int(11) NOT NULL COMMENT '优惠券id',
  `bill_id` int(11) NOT NULL COMMENT '账单id',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '记录日期',
  `content` varchar(255) DEFAULT '变更状态为已使用' COMMENT '内容说明',
  PRIMARY KEY (`id`,`coupon_id`,`bill_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `bill_id` (`bill_id`),
  CONSTRAINT `coupon_log_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`),
  CONSTRAINT `coupon_log_ibfk_2` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券使用记录表';

-- ----------------------------
-- Table structure for electricity
-- ----------------------------
DROP TABLE IF EXISTS `electricity`;
CREATE TABLE `electricity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '电表ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `value` decimal(10,2) DEFAULT '0.00' COMMENT '用电量',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '计费开始时间',
  PRIMARY KEY (`id`,`room_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  KEY `id` (`id`),
  CONSTRAINT `electricity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `electricity_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='电表';

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `file` varchar(255) DEFAULT NULL COMMENT '文件',
  `content` text COMMENT '日志内容',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '记录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `event_id` int(11) NOT NULL DEFAULT '0' COMMENT '产生消息的对象id',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '消息类型：comtract=合同消息，activity=活动消息，bill=账单消息，repair=报修消息，notice=通知公告，assessment=评价消息',
  `title` varchar(255) DEFAULT NULL,
  `detail` text,
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '发生日期',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态:0=未阅,1=已阅',
  PRIMARY KEY (`id`,`user_id`,`event_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息表';

-- ----------------------------
-- Table structure for preordain
-- ----------------------------
DROP TABLE IF EXISTS `preordain`;
CREATE TABLE `preordain` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '预定ID',
  `source_id` int(11) NOT NULL COMMENT '房源ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `name` varchar(255) DEFAULT NULL COMMENT '预定人姓名',
  `phone` varchar(11) DEFAULT NULL COMMENT '预定人电话',
  `card` varchar(255) DEFAULT NULL COMMENT '身份证',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '预定生成时间',
  `date_end` bigint(20) NOT NULL DEFAULT '0' COMMENT '预定结束时间',
  `content` text COMMENT '额外内容',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态,0=待交定金,1=已预定,2=已签约,3=未赴约,4=取消预订',
  PRIMARY KEY (`id`,`source_id`,`user_id`,`room_id`),
  KEY `source_id` (`source_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `preordain_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`),
  CONSTRAINT `preordain_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `preordain_ibfk_3` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预定表';

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '报修ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `area` varchar(255) DEFAULT NULL COMMENT '报修区域',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '报修发起时间',
  `date_start` bigint(20) NOT NULL DEFAULT '0' COMMENT '期望修复时间',
  `content` text COMMENT '文字说明',
  `pictures` varchar(255) DEFAULT NULL COMMENT '用户拍照，多张图片用逗号分隔',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态，0=等待维修，1=修复完成，2=取消',
  PRIMARY KEY (`id`,`user_id`,`room_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `repair_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '预约ID',
  `source_id` int(11) NOT NULL COMMENT '意向房源ID',
  `type_id` int(11) NOT NULL COMMENT '意向户型ID',
  `user_id` int(11) NOT NULL COMMENT '预约发起人ID',
  `name` varchar(255) DEFAULT NULL COMMENT '约看人姓名',
  `phone` varchar(11) DEFAULT NULL COMMENT '约看人手机号',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '预约发起时间',
  `date_start` bigint(20) NOT NULL DEFAULT '0' COMMENT '看房时间',
  `date_live` bigint(20) NOT NULL DEFAULT '0' COMMENT '期望入住时间',
  `budget` varchar(255) DEFAULT NULL COMMENT '房租预算',
  `content` varchar(255) DEFAULT NULL COMMENT '其他期望',
  `status` tinyint(4) DEFAULT '0' COMMENT '预约状态,0=已预约,1=已结束,2=未赴约,3=取消预约',
  PRIMARY KEY (`id`,`source_id`,`type_id`,`user_id`),
  KEY `source_id` (`source_id`),
  KEY `type_id` (`type_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`),
  CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `room_type` (`id`),
  CONSTRAINT `reservation_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预约表';

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房间id',
  `type_id` int(11) NOT NULL COMMENT '户型id',
  `name` varchar(255) DEFAULT NULL COMMENT '房间名',
  `code` varchar(255) NOT NULL DEFAULT '00' COMMENT '房间识别码,用来进行预定,最少两位',
  `password` char(6) NOT NULL DEFAULT '123456' COMMENT '房间密码',
  `floor` varchar(255) DEFAULT NULL COMMENT '所在楼层',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '房间状态，0=空闲,1=已预定,2=租赁中,10=不可用',
  PRIMARY KEY (`id`,`type_id`),
  KEY `type_id` (`type_id`),
  KEY `id` (`id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `room_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间表';

-- ----------------------------
-- Table structure for room_type
-- ----------------------------
DROP TABLE IF EXISTS `room_type`;
CREATE TABLE `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '户型ID',
  `souce_id` int(11) NOT NULL DEFAULT '0' COMMENT '房源ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `picture` varchar(255) DEFAULT NULL COMMENT '户型图片',
  `desc` text COMMENT '户型注释',
  `furniture` varchar(255) NOT NULL DEFAULT '000000000000' COMMENT '配套设施，多个用逗号分隔',
  `size` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '房间大小，单位为平方米',
  `type` varchar(255) DEFAULT NULL COMMENT '房间架构，例：一室一厅',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '房价',
  PRIMARY KEY (`id`,`souce_id`),
  KEY `id` (`id`),
  KEY `souce_id` (`souce_id`),
  CONSTRAINT `room_type_ibfk_1` FOREIGN KEY (`souce_id`) REFERENCES `source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='户型表';

-- ----------------------------
-- Table structure for source
-- ----------------------------
DROP TABLE IF EXISTS `source`;
CREATE TABLE `source` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房源id',
  `name` varchar(255) DEFAULT NULL COMMENT '房源名称',
  `desc` text COMMENT '房源介绍',
  `phone` varchar(11) DEFAULT NULL COMMENT '客服电话',
  `picture` varchar(255) DEFAULT NULL COMMENT '房源封面图',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市名',
  `coordinate` varchar(255) DEFAULT '' COMMENT '坐标，经纬度，使用逗号分隔',
  `nearby` varchar(255) DEFAULT '111111111111' COMMENT '周边设施',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `deposit` decimal(10,2) DEFAULT '500.00' COMMENT '定金',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态,0=正常营业,1=无法营业',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='房源列表';

-- ----------------------------
-- Table structure for suggestion
-- ----------------------------
DROP TABLE IF EXISTS `suggestion`;
CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '意见建议ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `content` text COMMENT '反馈内容',
  `date` int(11) NOT NULL DEFAULT '0' COMMENT '生成时间',
  PRIMARY KEY (`id`,`room_id`,`user_id`),
  KEY `room_id` (`room_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `suggestion_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `suggestion_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见反馈表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `username` varchar(255) DEFAULT NULL COMMENT '登陆名',
  `name` varchar(255) DEFAULT NULL COMMENT '用户展示名',
  `phone` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `desc` text COMMENT '个性签名',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户状态，0=冻结，1=正常',
  `date_create` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `third_account` varchar(255) DEFAULT NULL COMMENT '第三方账号',
  `third_name` varchar(255) DEFAULT NULL COMMENT '第三方名称，微博，微信，QQ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_auth
-- ----------------------------
DROP TABLE IF EXISTS `user_auth`;
CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `comp_id` int(11) NOT NULL COMMENT '认证机构ID',
  `name` varchar(255) DEFAULT NULL COMMENT '认证机构名',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否认证. 0=未认证, 1=已认证, 2=正在认证',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '认证时间',
  `card` varchar(255) NOT NULL DEFAULT '000000000000000000' COMMENT '身份证号',
  `card_type` varchar(255) DEFAULT '身份证' COMMENT '卡类型',
  PRIMARY KEY (`id`,`comp_id`),
  CONSTRAINT `user_auth_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for water
-- ----------------------------
DROP TABLE IF EXISTS `water`;
CREATE TABLE `water` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '水表ID',
  `room_id` int(11) NOT NULL COMMENT '房间ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `value` decimal(10,2) DEFAULT '0.00' COMMENT '用水量',
  `date` bigint(20) NOT NULL DEFAULT '0' COMMENT '抄表日期',
  PRIMARY KEY (`id`,`room_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  KEY `id` (`id`),
  CONSTRAINT `water_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `water_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='水表';
